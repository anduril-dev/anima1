function gray=forcegray(img)
% returns double grayscale image from RGB, RGBA, or GA images. 
% RGB to gray conversion via averaging channels
% gray=forcegray(img)
%     img = image of N dimenions, first 3 considered RGB

img=im2double(img);
if ndims(img)==3 % not grayscale
    if size(img,3)==4 % is RGBA
        img=img(:,:,1:3);
    end
    if size(img,3)==3 % is RGB
        img=mean(img,3);
    end
    if size(img,3)==2 % is GA
        img=img(:,:,1);
    end
end

gray=img;

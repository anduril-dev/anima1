function mask=ridofwrongshape(mask,minround,maxround,minecc,maxecc)
% Returns a binary mask, with objects that fit shape limits
% MASK=RIDOFWRONGSIZE(MASK,MINROUND,MAXROUND,MINECC,MAXECC)
%    MASK = binary image
%    MINROUND = minimum accepted roundness (0)
%    MAXROUND = maximum accepted roundness (1)
%    MINECC = minimum accepted eccentricity (0)
%    MAXECC = maximum accepted eccentricity (1)
%

    if all([minround==0 maxround==1 minecc==0 maxecc==1])
        return
    end
    masklab=bwlabel(mask,4);
    props=regionprops(masklab,'Area','Perimeter','Eccentricity');
    Areas=[props.Area];
    Round=sqrt(4*pi.*Areas./([props.Perimeter].^2));
    Ecc=[props.Eccentricity];
    goodround=intersect(find(Round<=maxround), find(Round>=minround));
    goodecc=intersect(find(Ecc<=maxecc), find(Ecc>=minecc));
    goodobjects=intersect(goodecc,goodround);
    mask=ismember(masklab,goodobjects);
end


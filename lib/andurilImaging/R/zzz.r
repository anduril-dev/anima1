.First.lib <- function(libname, pkgname) {
    pkgdir <- file.path(libname, pkgname)
    andurilImaging.set.package.dir(pkgdir)
}

.onAttach <- function(libname,pkgname){
    ver <- read.dcf(file=system.file("DESCRIPTION", package=pkgname),
           fields=c("Version", "Date"))
   packageStartupMessage(paste(pkgname, ver[1], "\t", ver[2], "\n"))
          invisible(NULL)
}

.onUnload <- function(libpath){
            invisible(NULL)
}


### Package tools ######################################################

if (!exists('.andurilImaging.package.dir')) {
    .andurilImaging.package.dir <<- NULL
}

andurilImaging.set.package.dir <- function(directory) {
    .andurilImaging.package.dir <<- directory
}

andurilImaging.get.package.dir <- function() {
    return(.andurilImaging.package.dir)
}

andurilImaging.package.file.path <- function(...) {
    pkg.dir <- andurilImaging.get.package.dir()
    if (is.null(pkg.dir)) {
        stop(sprintf('Directory for package andurilImaging is not set'))
    }
    return(file.path(pkg.dir, ...))
}

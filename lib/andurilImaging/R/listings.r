
imaging.get.files <- function(directory, full.names=TRUE, pattern=NULL, ignore.case=TRUE) {
    if (is.null(pattern)) {
        pattern<-IMGEXTENSIONLIST
    }
    return(list.files(directory, pattern=pattern, full.names=full.names, ignore.case=ignore.case))
}

#!/bin/bash
set -e

NAME=Fiji.app
VERSION=""
URL="http://jenkins.imagej.net/job/Stable-Fiji/lastSuccessfulBuild/artifact/fiji-linux64.tar.gz"
cd "$( dirname $0 )"/..
[[ -d "${NAME}" ]] && {
    echo "${NAME} already installed"
    # Update
    cd ${NAME}
    ./ImageJ-linux64 --update update
    chmod ugo+rX .
    exit 0
}

rm -rf "${NAME}"
wget -O - $URL | tar zx
chmod ugo+rX "${NAME}"

#ln -Tsf "${NAME}-${VERSION}" $NAME


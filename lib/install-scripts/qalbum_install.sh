#!/bin/bash
set -e

NAME=qalbum
URL="https://bitbucket.org/MoonQ/qalbum/"
cd "$( dirname $0 )"/..
[[ -d "${NAME}" ]] && {
    echo "${NAME} already installed, updating"
    cd ${NAME}
    hg pull 
    EC=$?
    hg up -C
    exit $EC
}

hg clone "$URL" $NAME

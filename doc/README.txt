# Anima

Anima is an image analysis workflow environment on top of Anduril.

Anima acts as a superplatform for the existing imaging applications, 
and can run components and software written in Java, Bash (i.e. any binary), 
Perl, Python, R, or Matlab. In addition Anima provides API to the 
batch environments of Fiji, CellProfiler and ImageMagick.

See installation and other information from:
http://anduril.org/anima/

Read about Anima:
Anima: Modular workflow system for comprehensive image data analysis. 
Ville Rantanen, Miko Valori and Sampsa Hautaniemi, 
Front. Bioeng. Biotechnol. doi: 10.3389/fbioe.2014.00025
http://journal.frontiersin.org/Journal/10.3389/fbioe.2014.00025/


#!/bin/bash

NETWORK_TO_RUN=ImageAnalysis.and
RESULTS_IN=results

# you have to define where Anduril and the Imaging bundle are!
#ANDURIL_HOME=/usr/share/anduril/
#IMAGINGBUNDLE=/usr/share/anduril-bundles/anima/

if [ -f "${ANDURIL_HOME}/anduril.jar" ] && [ -f "${IMAGINGBUNDLE}/bundle.xml" ]
then echo running...
    $ANDURIL_HOME/bin/anduril run $NETWORK_TO_RUN -d $RESULTS_IN -b $IMAGINGBUNDLE --threads 2
else
echo "Anduril or Imaging bundle not defined or found!"
echo "Please see that ANDURIL_HOME and IMAGINGBUNDLE variables are set. (see this script)"
fi



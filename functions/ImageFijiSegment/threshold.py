from ij.process import ImageProcessor
import ij.Prefs

method = param1
correction = float(param2)
ij.Prefs.blackBackground = True
IJ.setAutoThreshold(im1, method)
ip = im1.getProcessor()
threshold = ip.getMinThreshold() * correction
ip.setThreshold(threshold, ip.getMaxThreshold(), ImageProcessor.NO_LUT_UPDATE)
IJ.run(im1, "Convert to Mask", "")
imout=im1
# perimeters
imout2=imageCopy(im1)
IJ.run(imout2, "Outline", "")


simsize=param1;
steps=param2;
fun=eval(param3);
tic
p=fileparts(scriptfile);
addpath(p);

%% directional similarity:
block=[simsize simsize];
%fun=@mean;
%im1=1+im1*(steps-1);
[h w]=size(im1);
cx=(block(2)+1)/2; cy=(block(1)+1)/2;
ci=sub2ind(block,cy,cx);
blk=@(x) direction_difference(x,steps,ci,fun);
similar_direction=zeros(size(im1));

for X=cx:(w-cx+1)
for Y=cy:(h-cy+1)
    similar_direction(Y,X)=blk(im1((Y-cy+1):(Y+cy-1),(X-cx+1):(X+cx-1)));
end
end

[Y,X]=ind2sub([h w],1:(h*w));

imout=mat2gray(similar_direction);
tableout.columnheads={'Y','X','SimilarityValue'};
tableout.data=[num2csvcell([Y', X'],'%d'), num2csvcell(similar_direction(:)) ];
toc

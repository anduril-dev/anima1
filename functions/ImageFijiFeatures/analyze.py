from ij.process import ImageProcessor
from ij.process import ImageStatistics as IS
from ij.plugin.frame import RoiManager
from ij.measure import ResultsTable
from ij.plugin.filter import ParticleAnalyzer
from ij.plugin.filter import Analyzer
from ij.measure import Measurements as M
from java.lang import Double
import ij.Prefs
import csv

measurements = M.AREA | M.AREA_FRACTION | M.CENTER_OF_MASS | M.CENTROID | M.CIRCULARITY | M.ELLIPSE | M.FERET | M.INTEGRATED_DENSITY | M.KURTOSIS | M.MAX_STANDARDS | M.MEAN | M.MEDIAN | M.MIN_MAX | M.MODE | M.PERIMETER | M.RECT | M.SKEWNESS | M.STD_DEV
ij.Prefs.blackBackground = True

# Make sure im2 is a thresholded image
IJ.run(im2, "8-bit", "");
IJ.setThreshold(im2,127, 255)
IJ.run(im2, "Convert to Mask", "")

ip = im2.getProcessor()
options = IS.MIN_MAX  
stats = IS.getStatistics(ip, options, im2.getCalibration())  

if stats.max>0.0:

    # Add segments from mask image (im2) to ROI Manager
    minSize = int(param1)
    minCirc = float(param2)
    fourconnected = param3
    options = ParticleAnalyzer.ADD_TO_MANAGER
    if fourconnected != "false":
        options |= ParticleAnalyzer.FOUR_CONNECTED
    pa = ParticleAnalyzer(options, 0, None, minSize, Double.POSITIVE_INFINITY, minCirc, 1.0)
    pa.analyze(im2)


    # Collect measurements from input image (im1) into output table
    roim = RoiManager.getInstance()
    rtable = ResultsTable()
    analyzer = Analyzer(im1, measurements, rtable)
    for roi in roim.getRoisAsArray():
      im1.setRoi(roi)
      stats = im1.getStatistics(measurements)
      analyzer.saveResults(stats, roi)


    filename = os.path.join(dirtblout, outfilename + extension)
    rtable.saveAs(filename + ".txt") # .txt extension gives tab separated fields
    roim.runCommand("Delete")

    # Initialize headers
    reader = csv.reader(open(filename + ".txt",'rb'),delimiter='\t')
    for row in reader:
        break
    header=[]
    header.extend(['RowId','Object'])
    header.extend(row)
    header.remove(' ')
    # open reader and writer    
    reader = csv.DictReader(open(filename + ".txt",'rb'),delimiter='\t')    
    writer = csv.DictWriter(open(filename + ".csv",'wb'),header,delimiter='\t',
                            quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict(zip(header,header)))
    unique_pos=[]
    object_no=0
    for row in reader:
        # remove sometimes occurring duplicates (when fourconnected)
        new_pos='x'.join([ row['X'], row['Y'], row['Area'] ])
        if new_pos in unique_pos:
            continue
        unique_pos.append(new_pos)
        object_no+=1
        row['RowId']=outfilename + extension + '_%d' % object_no
        row['Object']=str(object_no)
        del row[' ']
        writer.writerow(row)

    os.remove(filename + ".txt")

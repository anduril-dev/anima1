
label=param2;
% Summary columns are expected as default head names..
slabel='File';
w='Width';
h='Height';

outdir=getoutput(cf,'optOut1');
mkdir(outdir);
clear optOut1;
table.data=zeros(1);
table.columnheads={'0'};

clabels=getcellcol(table1,label,'string');
slabels=getcellcol(table2,slabel,'string');
sw=getcellcol(table2,w,'float');
sh=getcellcol(table2,h,'float');

remain=param1;
cs=zeros(numel(clabels),0);
colnames=cell(0);

while true
    [col, remain] = strtok(remain, ',');
    col=strtrim(col);
    if isempty(col),  break;  end
    colnames=[colnames {col}];
end
for i=1:2
for col=colnames
    cs=[cs floor(getcellcol(table1,[col{1} num2str(i)],'float'))];
end
end

for im=1:numel(slabels)
    imrows=strcmp(clabels,slabels{im});
    imsize=[sh(im),sw(im)];
    imout=false(imsize);
    imseg=floor(cs(imrows,:));
    for seg=1:size(imseg,1)
        accuracy=1+max(abs(imseg(seg,1)-imseg(seg,3)),abs(imseg(seg,2)-imseg(seg,4)));
        Xvec=floor(linspace(imseg(seg,1),imseg(seg,3),accuracy));
        Yvec=floor(linspace(imseg(seg,2),imseg(seg,4),accuracy));
        Ivec=sub2ind(imsize,Yvec,Xvec);
        imout(Ivec)=true;
    end
    if strcmpi(slabels{im}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    imwrite(logical(imout),[outdir filesep slabels{im} pngstr])
end





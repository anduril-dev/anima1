#!/usr/bin/python
import sys
import anduril.imagetools as ait
from anduril.args import *
import anduril

im=ait.load_image(image,flatten=1)
if (mask!=None):
    mask_im=ait.load_image(mask,flatten=1)
    if (mask_im.shape!=im.shape):
        write_error('Mask and Image are of different size.')
        sys.exit(1)

writer=anduril.TableWriter(csv,fieldnames=['X','Y','Intensity'])
if (mask==None):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            writer.writerow({'X':x,
                             'Y':y,
                             'Intensity':im[y,x]})
else:
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            if (mask_im[y,x]==1):
                writer.writerow({'X':x,
                             'Y':y,
                             'Intensity':im[y,x]})
            else:
                writer.writerow({'X':x,
                             'Y':y,
                             'Intensity':'NA'})






source "$ANDURIL_HOME/bash/functions.sh"
export_command

mkdir "${output_mask}"

IFS=$'\n'
for dir in $( ls "${input_masklist}" ); 
do echo "$dir"
   for file in $( ls "${input_masklist}"/"${dir}" ) 
   do i=$( echo $file | tr -d -c [:digit:] )
      cp "${input_masklist}"/"${dir}"/"${file}" "${output_mask}"/"${i}_${dir}"
   done
done


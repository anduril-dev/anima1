function skel2line(cf)

tic
imdir=getinput(cf,'skeleton');
graydir=getinput(cf,'gray');
imorigdir=getinput(cf,'origdir');

outfile=getoutput(cf,'table');
outdir=getoutput(cf,'keypoints');

maxdist=getparameter(cf,'maxdist','float');

imglist=getimagelist(imdir);
graylist=getimagelist(graydir);
if ~inputdefined(cf,'origdir')
    origimglist=graylist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Source directories (skeleton,origdir) have different number of images')
    return
end
if numel(imglist)~=numel(graylist)
    writeerror(cf,'Source directories (skeleton,gray) have different number of images')
    return
end
if numel(imglist)==0
    writeerror(cf,'No images in source directory')
    return
end

writeout.columnheads={'RowId','File','Edge','Length','X1','Y1','X2','Y2', ...
                      'Alpha','Endpoint','Branchpoint','X','Y','Distance','Mean','Median','Sum', ...
                      'StDev','Min','Max','Fit X2','Fit X1','Fit X0','Fit error'};
mkdir(outdir);

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    gid=[graydir filesep graylist{file}];
    disp(['Read image: ' graylist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    skel=logical(im2double(imread(id)));
    gray=im2double(imread(gid));
    try
        if ~all(size(gray)==size(skel))
            writeerror(cf,['Images are of different size: ' id ' vs. ' gid]);
        end
    catch me
        writeerrorstack(cf, me);
        exit
    end
    dataout=skelwalker(gray,skel,maxdist);
    
    dataout=num2csvcell(dataout);
    objs=size(dataout,1);
    dataoutname=repmat(origimglist(file),[objs 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);    
    % handling the output image
    endimg=bwmorph(skel,'endpoints');
    braimg=bwmorph(skel,'branchpoints');
    outimg=endimg+0.5.*braimg;
    imwrite(outimg,[outdir filesep graylist{file} pngstr]);
end

toc
end

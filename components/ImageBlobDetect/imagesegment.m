function imagesegment(cf)
%
% cf = Anduril command file

imdir=getinput(cf,'dir');
%outdir=getvariable(cf,'output.visu');
outcsv=getoutput(cf,'objects');
imorigdir=getinput(cf,'origdir');

threshold=getparameter(cf,'threshold','float');
eigenth=getparameter(cf,'mineigen','float');
mina=getparameter(cf,'minsize','float');
maxa=getparameter(cf,'maxsize','float');
steps=getparameter(cf,'steps','float');

tic
imglist=getimagelist(imdir);
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

writeout.columnheads={'RowId' 'File' 'Object' 'X' 'Y' 'Rad' 'Alpha' 'Lx' 'Ly' 'Eccentricity'};
totalobjs=0;
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    gray=im2double(imread(id));
    if ndims(gray)==2
    data=blobdetect(gray,threshold,mina,maxa,eigenth,steps);
    
    objs=size(data,1);
    fn=repmat(origimglist(file),[objs 1]);
    objnums=num2cell((1:objs)');
    objnums=cellfun(@(x) sprintf('%g',x), objnums,'UniformOutput',false);
    filename=[strcat(fn,'_',objnums) fn objnums];
    else
        disp([ imglist{file} ' image is not gray scale!'])
    end
    if numel(data)>0
        writeout.data=[filename num2csvcell(data)];
    else
        writeout.data={};
    end
    appendcsv(outcsv,writeout);    
    
    fprintf('%d of %d done.\n',file,numel(imglist))
    totalobjs=totalobjs+objs;
end

if totalobjs==0
	writelog(cf,'No blobs found in any images. You may have to relax parameters (threshold and mineigen) ');
end

toc
end


source "$ANDURIL_HOME/bash/functions.sh"
export_command
# make the list separator a rowchange
IFS=$'\n' 

indir1=$( getinput dir1 )
indir2=$( getinput dir2 )
indir3=$( getinput dir3 )
indir4=$( getinput dir4 )
infile1=$( getinput file1 )
infile2=$( getinput file2 )
inarraydirs=( $( getarrayfiles dirArray ) )
outdir=$( getoutput dir )
switches=$( getparameter command ) 
switchfile=$( getinput command ) 
outext=$( getparameter extension )
inext=$( getparameter oldExtension ) 
infilter=$( getparameter filter )
magickbin=$( getparameter binary )
tempdir=$( gettempdir )
failmiss=$( getparameter failOnMissing )
files=$( getparameter files )
parallel=$( getparameter parallel )

CONVERT=$( which $magickbin )
if ( "$CONVERT" -version | grep -i imagemagick )
    then echo "ImageMagick found."
else
    msg="ImageMagick $magickbin not found. ( $CONVERT )"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi
if [ -f "$switchfile" ]
    then cp "$switchfile" "$tempdir"/magicksource.sh
else    
    echo -e "$switches" > "$tempdir"/magicksource.sh
fi

if ( grep "@out@" "$tempdir"/magicksource.sh > /dev/null )
    then echo "@out@ port found"
else
    msg="@out@ not present in the script!"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi

# list files
if [ -d "$input_array1" ]
then inafiles1=( $( getarrayfiles array1 ) ) || echo "error reading input array1" >> "$errorfile"
fi
if [ -d "$input_array2" ]
then inafiles2=( $( getarrayfiles array2 ) ) || echo "error reading input array2" >> "$errorfile"
fi
if [ -d "$indir1" ]
then infiles1=( $( ls -v -1 "$indir1" | grep -i "$infilter" ) ) || echo "error reading input dir1" >> "$errorfile"
fi
if [ -d "$indir2" ]
then infiles2=( $( ls -v -1 "$indir2" | grep -i "$infilter" ) ) || echo "error reading input dir2" >> "$errorfile"
fi
if [ -d "$indir3" ]
then infiles3=( $( ls -v -1 "$indir3" | grep -i "$infilter" ) )
fi
if [ -d "$indir4" ]
then infiles4=( $( ls -v -1 "$indir4" | grep -i "$infilter" ) )
fi
if [ -d "$infile1" ]
then infilelist1=( $( ls -v -1 "$infile1" | grep -i "$infilter" ) )
fi
if [ -d "$infile2" ]
then infilelist2=( $( ls -v -1 "$infile2" | grep -i "$infilter" ) )
fi

# loop over array inputs and list files
for (( a=0; a<${#inarraydirs[@]}; a++ ))
do aidx=$(( $a + 1 ))
   thisfiles=( $( ls -v -1 "${inarraydirs[$a]}" | grep -i "$infilter" ) )
   #echo input $aidx , ${inarraydirs[$a]} : ${#thisfiles[@]}
   for (( f=0; f<${#thisfiles[@]}; f++ ))
   do eval arrayfiles${aidx}[$f]=\"${thisfiles[$f]}\"
   done
done
# Filenaming based on dir1, array1 or dirArray[1]
# dirArray is the source
for (( f=0; f<${#arrayfiles1[@]}; f++ ))
do srcfiles[$f]="${outdir}/${arrayfiles1[$f]}"
done
if [ -d "$input_array1" ]
then # array1 is the source
    srcfiles=( $( getarraykeys array1 ) )
    for (( f=0; f<${#srcfiles[@]}; f++ ))
    do srcfiles[$f]="${outdir}/${srcfiles[$f]}"
    done
fi
if [ -d "$indir1" ]
then # dir1 is the source
    for (( f=0; f<${#infiles1[@]}; f++ ))
    do srcfiles[$f]="${outdir}/${infiles1[$f]}"
    done
fi

if [ $files -gt 0 ];
then for (( f=0; f<$files; f++ ))
     do [ -z "srcfiles[$f]" ] || srcfiles[$f]="${outdir}/file"$( printf "%0${#files}d" $(( $f + 1 )) )
     done
else files=${#srcfiles[@]}
fi

# create directories
mkdir "${outdir}" || echo error creating directory ${dir[$d]} >> "$errorfile"
pad=${#files}
# loop over images
for (( f=0; f<$files; f++ ))
    do newname="${srcfiles[$f]/%$inext/}${outext}"
       newbase=$( basename "${newname}" )
       oldbase=$( basename "${srcfiles[$f]}" )
    fpad=$( printf "%0${pad}d" $f )
    fpadplus=$( printf "%0${pad}d" $(( $f+1 )) )
    echo echo "\"${fpadplus} / ${files} : ${oldbase} -> ${newbase}\"" > "${tempdir}/magickrun.sh"
    echo -n \"${CONVERT}\"" " >> "${tempdir}/magickrun.sh"
    sed -e 's,@in1@,"'"${indir1}/${infiles1[$f]}"'",g' \
    -e 's,@in2@,"'"${indir2}/${infiles2[$f]}"'",g' \
    -e 's,@in3@,"'"${indir3}/${infiles3[$f]}"'",g' \
    -e 's,@in4@,"'"${indir4}/${infiles4[$f]}"'",g' \
    -e 's,@af1@,"'"${inafiles1[$f]}"'",g' \
    -e 's,@af2@,"'"${inafiles2[$f]}"'",g' \
    -e 's,@dir1@,"'"${indir1}/"'",g' \
    -e 's,@dir2@,"'"${indir2}/"'",g' \
    -e 's,@dir3@,"'"${indir3}/"'",g' \
    -e 's,@dir4@,"'"${indir4}/"'",g' \
    -e 's,@f1@,"'"${infile1}/${infilelist1[0]}"'",g' \
    -e 's,@f2@,"'"${infile2}/${infilelist2[0]}"'",g' \
    -e 's,@out@,"'"${newname}"'",g' \
    -e 's,@filenr@,'$(( $f + 1 ))',g' \
    -e 's,\r$,,' "${tempdir}/magicksource.sh" >> "${tempdir}/magickrun.sh"
    inall=""
    for (( a=0; a<${#inarraydirs[@]}; a++ ))
    do aidx=$(( $a + 1 ))
        mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun.sh.tmp"
        # dirty trick to use dynamic array variables...
        sed -e 's,@ina'$aidx'@,"'"${inarraydirs[$a]}/"$(eval "echo \${$(echo arrayfiles${aidx}[$f])}")'",g' \
        "${tempdir}/magickrun.sh.tmp" >> "${tempdir}/magickrun.sh"
        mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun.sh.tmp"
        sed -e 's,@dira'$aidx'@,"'"${inarraydirs[$a]}/"'",g' \
        "${tempdir}/magickrun.sh.tmp" >> "${tempdir}/magickrun.sh"
        inall=$inall" \"${inarraydirs[$a]}/$(eval "echo \${$(echo arrayfiles${aidx}[$f])}")\""
    done    
    sed 's,@inall@,'"$inall"',g' -i "${tempdir}/magickrun.sh"
    #cat "${tempdir}/magickrun.sh" >> "$logfile"
    mv "${tempdir}/magickrun.sh" "${tempdir}/magickrun${fpad}.sh"
    rm -f "${tempdir}/magickrun.sh.tmp"

    echo $slsep >> "$logfile"
done

fpad=$( printf "%0${pad}d" 0 )
cat "${tempdir}/magickrun${fpad}.sh"  
rm "${tempdir}/magicksource.sh"
cd "$tempdir"
# parallelization
ls -v | xargs -I SCRIPT --max-procs=$parallel bash SCRIPT

for (( f=0; f<$files; f++ ))
    do newname="${srcfiles[$f]/%$inext/}${outext}"
    if [ "$failmiss" = "true" ]
    then if [ ! -f "${newname}" ]
            then echo "error converting file (${srcfiles[$f]})" >> "$errorfile"
         fi
    fi
done

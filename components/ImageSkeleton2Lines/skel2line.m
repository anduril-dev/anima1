function skel2line(cf)

tic
imdir=getinput(cf,'skeleton');
imorigdir=getinput(cf,'origdir');

outfile=getoutput(cf,'table');

maxdist=getparameter(cf,'maxdist','float');

imglist=getimagelist(imdir);
if ~inputdefined(cf,'origdir')
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Source directories have different number of images')
    return
end
if numel(imglist)==0
    writeerror(cf,'No images in source directory')
    return
end

writeout.columnheads={'RowId','File','Edge','Length','X1','Y1','X2','Y2', ...
                      'Alpha','Endpoint','Branchpoint'};

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    skel=logical(im2double(imread(id)));
    dataout=skelwalker(skel,maxdist);
    
    dataout=num2csvcell(dataout);
    objs=size(dataout,1);
    dataoutname=repmat(origimglist(file),[objs 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);    
end

toc
end

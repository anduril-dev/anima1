function list=skelwalker(skel,maxdist)
    if maxdist==0
        maxdist=inf;
    end
    imsize=size(skel);
    skel=cleanborders(skel,imsize);
    list=zeros(0,9);
    startps=zeros(0);
    %'Edge','Length','X1','Y1','X2','Y2',
    %'Alpha','Endpoint','Branchpoint'
    
    endimg=logical(bwmorph(skel,'endpoints'));
    braimg=logical(bwmorph(skel,'branchpoints'));
    skelout=skel;
    
    [branch_y,branch_x]=find(braimg==1);
    [ends_y,ends_x]=find(endimg==1);
    endps=find(endimg);
    branchps=find(braimg);
    close all;
    newskel=skel;
    seg=1;
    for e=1:numel(endps)
        newskel([endps;branchps])=true;
        [pair,newskel,walked]=firstpair(newskel,endps,branchps,endps(e),maxdist);
        if pair==endps(e)
            continue
        end
        list(seg,1)=seg;
        list(seg,2)=walked;
        list(seg,3:4)=ind2coo(imsize,endps(e));
        list(seg,5:6)=ind2coo(imsize,pair);
        list(seg,8)=any(ismember([endps(e) pair],endps));
        list(seg,9)=any(ismember([endps(e) pair],branchps));
        startps(seg)=endps(e);
        seg=seg+1;
        if walked>maxdist
           % Generated branch gets added
            branchps=[pair; branchps];
        end
    end
    newskel(endps)=false;
    while numel(branchps)>0
        newskel(branchps)=true;
        [pair,newskel,walked]=firstpair(newskel,endps,branchps,branchps(1),maxdist);
        if walked==1
            branchps(1)=[];
            continue
        end
        list(seg,1)=seg;
        list(seg,2)=walked;
        list(seg,3:4)=ind2coo(imsize,branchps(1));
        list(seg,5:6)=ind2coo(imsize,pair);
        list(seg,8)=any(ismember([branchps(1) pair],endps));
        list(seg,9)=any(ismember([branchps(1) pair],branchps));
        startps(seg)=branchps(1);
        seg=seg+1;
        if walked>maxdist
           % Generated branch gets added
            branchps=[pair; branchps];
        end
    end
    [sortstarts,sortidx]=sort(startps);
    list=list(sortidx,:);
    list(:,1)=1:size(list,1);
    list(:,7)=(-atan2(list(:,6)-list(:,4),list(:,5)-list(:,3)));
end

function coords=ind2coo(s,ind)
    [y,x]=ind2sub(s,ind);
    coords=[x y];
end

function bw=cleanborders(bw,siz)
    bw(1,:)=0;
    bw(:,1)=0;
    bw(siz(1),:)=0;
    bw(:,siz(2))=0;
end

function [pair,skel,walked]=firstpair(skel,endps,branchps,p,maxdist)
    found=false;
    pos=p;
    M=size(skel,1);
    N=[-1 M-1 M M+1 +1 -M+1 -M -M-1];
    Norient=N;
    endps=endps(p~=endps);
    branchps=branchps(p~=branchps);
    possible_hits=[endps;branchps];
    walked=1;
    while ~found
        skel(pos)=false;
        %imshow(skel); drawnow;
        neighbors=pos+Norient;
        if any(ismember(neighbors,possible_hits))
            found=true;
            pos=neighbors(ismember(neighbors,possible_hits));
            pos=pos(1);
            break;
        end
        newhit=find(skel(neighbors));
        if numel(newhit)==0
            pos=p;
            break;
        end
        pos=neighbors(newhit(1));
        if newhit(1)~=1
            Norient=circshift(Norient,[1 1-newhit(1)]);
        end
        if walked>maxdist
            break;
        end
        walked=walked+1;
    end
    pair=pos;
end

addpath('../../lib/matlab/')
% commandfile
try
    cf=readcommandfile(commandfile);  


tic
maskdir=getinput(cf,'dir');
origdir=getinput(cf,'origdir');
datafile=getinput(cf,'csv');

outdir=getoutput(cf,'dir');

masklist=getimagelist(maskdir);
if strcmp(origdir,'')
    origlist=masklist;
    origdir=maskdir;
else
    origlist=getexoticimagelist(origdir);
end

filecolname=getparameter(cf,'fileCol','string');
numberonly=getparameter(cf,'textOnly','boolean');
xcolname=getparameter(cf,'xCol','string');
ycolname=getparameter(cf,'yCol','string');
valuecolname=getparameter(cf,'valueCol','string');
xoffset=getparameter(cf,'xOffset','float');
yoffset=getparameter(cf,'yOffset','float');
format=getparameter(cf,'format','string');
scale=getparameter(cf,'scale','float'); scale=max(1,scale);
scalemethod=getparameter(cf,'scaleMethod','string');
inverted=getparameter(cf,'invert','boolean');

if numel(masklist)~=numel(origlist)
    writeerror(cf,'dir and origdir image directories have different number of images')
    return
end

mkdir(outdir);

data=readcsvcell(datafile,0,char(9));

files=getcellcol(data,filecolname,'string');
X=(getcellcol(data,xcolname,'float')+xoffset);
Y=(getcellcol(data,ycolname,'float')+yoffset);
if iscellcol(data,valuecolname)
if strcmp(format,'%s')
    values=getcellcol(data,valuecolname,'string');
else 
    values=getcellcol(data,valuecolname,'float');
    values=arrayfun(@(x) sprintf(format,x), values,'UniformOutput',false);
end
else
    writelog(cf,['Column "' valuecolname '" not found in table, using it as string ']);
    values=repmat({valuecolname},size(files));
end
for file=1:numel(masklist)
    idorig=origlist{file};
    idmask=[maskdir filesep masklist{file}];
    disp(['Read image: ' masklist{file} ' (' num2str(file) '/' num2str(numel(masklist)) ')']);
    % Read mask image
    mask=(im2double(imread(idmask)));
    
    imout=zeros(size(mask,1),size(mask,2));
    filerows=strcmp(files, idorig);
    if sum(filerows)==0
        writelog(cf,['File not found in list: ' idorig]);
    end
    filevalues=values(filerows);
    filex=X(filerows);
    filey=Y(filerows);
    for o=1:numel(filevalues)
        imout=max(imout,writeonimage(imout,filevalues{o},filex(o),filey(o),scale,scalemethod));
    end
    if numberonly
        if inverted
            imwrite(imcomplement(imout),[outdir filesep masklist{file}]);
        else
            imwrite(imout,[outdir filesep masklist{file}]);
        end
    else
        imout=repmat(imout,[1 1 size(mask,3)]);
        if inverted
            imout=min(mask,imcomplement(imout));
        else
            imout=max(mask,imout);
        end
        imwrite(imout,[outdir filesep masklist{file}]);
    end

end

catch me
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  % the script must exit matlab at the end!


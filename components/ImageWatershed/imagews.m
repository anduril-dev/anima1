function imagews(cf)
%
% cf = Anduril command file

maskdir=getinput(cf,'images');
seeddir=getinput(cf,'seed');
outmaskdir=getoutput(cf,'mask');
outlinesdir=getoutput(cf,'lines');
outperimdir=getoutput(cf,'perimeter');
masklist=getimagelist(maskdir);
seedlist=getimagelist(seeddir);

blurw=getparameter(cf,'blur','float');
sharpenw=getparameter(cf,'sharpen','float');
mina=getparameter(cf,'minsize','float');
maxa=getparameter(cf,'maxsize','float');
invert=getparameter(cf,'invert','boolean');
method=lower(getparameter(cf,'method','string'));
  switch method
        case 'ds'

        case 'ws'

        case 'vo'
            if ~exist(seeddir,'dir')
                writeerror(cf,'You must give initial seed masks.')
                return
            end
            if (numel(masklist) ~= numel(seedlist))
                writeerror(cf,'Mask dir and Seed dir contain different number of images.')
                return
            end
        case 'de'
            if ~exist(seeddir,'dir')
                writeerror(cf,'You must give initial seed masks.')
                return
            end
            if (numel(masklist) ~= numel(seedlist))
                writeerror(cf,'Mask dir and Seed dir contain different number of images.')
                return
            end
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
            return
    end

mkdir(outmaskdir);
mkdir(outlinesdir);
mkdir(outperimdir);

tic
for file=1:numel(masklist)
    id=[maskdir filesep masklist{file}];
    disp(['Read image: ' masklist{file} ' (' num2str(file) '/' num2str(numel(masklist)) ')']);
    gray=im2double(imread(id));
    orig_gray=gray;
    if invert
        gray=imcomplement(gray);
        orig_gray=imcomplement(orig_gray);
    end
    if method=='ds'
        gray=mat2gray(bwdist(imcomplement(logical(mat2gray(gray)))));
        orig_gray=logical(orig_gray);
    end
    if blurw>0
        w=round(6*blurw);
        gray=imfilter(mat2gray(gray),fspecial('gaussian',w,blurw));
    end
    if sharpenw>0
        w=round(6*sharpenw);
        gray=mat2gray(gray - imfilter(gray,fspecial('gaussian',w,sharpenw)));
    end

    mask=false(size(gray));
    if strcmpi(masklist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    switch method
        case 'ds'
            lines=~watershed(imcomplement(gray));
            lines=bwmorph(lines,'thin',inf);
            mask=orig_gray.*(~lines);
        case 'ws'
            lines=~watershed(imcomplement(gray));
            lines=bwmorph(lines,'thin',inf);
            mask=orig_gray.*(~lines);
        case 'vo'
            seedid=[seeddir filesep seedlist{file}];
            seed=im2double(imread(seedid));
            [mask,lines]=voronoicut(orig_gray,seed);
        case 'de'
            seedid=[seeddir filesep seedlist{file}];
            seed=im2double(imread(seedid));
            [mask,lines]=delaunaycut(orig_gray,seed);
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
    end
    mask=ridofwrongsize(mask,mina,maxa);
    imwrite((mask),[outmaskdir filesep masklist{file} pngstr])
    imwrite(logical(lines),[outlinesdir filesep masklist{file} pngstr])
    imwrite(logical(bwperim(mask,4)),[outperimdir filesep masklist{file} pngstr])
end
toc
end


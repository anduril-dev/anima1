function [mask,lines]=delaunaycut(gray,seed)

props=regionprops(bwlabel(seed,4),'Centroid');
centers=reshape([props.Centroid],[2,numel(props)])';
vx=centers(:,1);
vy=centers(:,2);
lines=false(size(seed));
edges=[1 2; 2 3; 3 1]';
try
    tri=delaunay(vx,vy);
    [h,w]=size(lines);
    for vert=1:size(tri,1)
        for edge=edges
            % pixels needed to draw the line
            x1=vx(tri(vert,edge(1)));
            x2=vx(tri(vert,edge(2)));
            y1=vy(tri(vert,edge(1)));
            y2=vy(tri(vert,edge(2)));
            
            pixels=3*max(abs(x1-x2),abs(y1-y2));
            pix_x=floor(linspace(x1,x2,pixels));
            pix_y=floor(linspace(y1,y2,pixels));
            pix_x=min(w,pix_x); pix_x=max(1,pix_x);
            pix_y=min(h,pix_y); pix_y=max(1,pix_y);
            ind=sub2ind([h w],pix_y,pix_x);
            lines(ind)=true;
        end
    end
catch
    
end
lines=bwmorph(lines,'thin',inf);
mask=imcomplement(lines).*gray;

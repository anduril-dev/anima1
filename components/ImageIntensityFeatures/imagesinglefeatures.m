function [dataout,imout]=imagesinglefeatures(cf,id,idmask,erodewidth,erodelengthproportion,percentile,masklist,connectivity,texturescale)

gray=im2double(imread(id));
mask=im2double(imread(idmask));
if size(gray,3)~=1
    writeerror(cf,['Source gray file not gray scale: ' id])
    return
end
if size(mask,3)~=1
    writeerror(cf,['Source mask file not gray scale: ' idmask])
    return
end
if exist(masklist,'dir')
    maskimages=getimagelist(masklist);
else
    maskimages=false;
end
if iscell(maskimages)
    disp(['Using MaskList with ' num2str(numel(maskimages)) ' objects']);
    props=struct('Centroid',{},'BoundingBox',{},'MajorAxisLength',{},'MinorAxisLength',{},...
        'Image',{});
    for s=1:numel(maskimages)
        maskim=imread([masklist filesep num2str(s) '.png']);
        props(s,1)=regionprops(maskim,'Image','Centroid','BoundingBox','MajorAxisLength','MinorAxisLength');
        if s==1
            mask=maskim;
        else
            mask=or(mask,maskim);
        end
    end
else
    props=regionprops(bwlabel(mask,connectivity),'Image','Centroid','BoundingBox','MajorAxisLength','MinorAxisLength');
end
imout=zeros(size(mask));
%props=regionprops(bwlabel(mask,4),'Image','Centroid','BoundingBox','MajorAxisLength','MinorAxisLength');
objs=size(props,1);

if objs>0
    objnums=(1:objs)';
    coords=reshape([props.Centroid], [2 objs])';
    intens=zeros(objs,34);
    erodew=round(erodewidth);
    if erodew<0
        bigfill=padarray(zeros(size(mask)),[-erodew -erodew],0,'both');
    end
    for obj=1:objs
        cds=ceil(props(obj).BoundingBox);  % bounding box coordinates (x,y,w,h)
        fill=double(props(obj).Image);        
        fill(fill==0)=nan;
        objimg=fill.*gray(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
        %     objimg=props(obj).FilledImage.*im1(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
        % make sure objvals is always Nx1 vector
        objvals=objimg(~isnan(objimg)); objvals=objvals(:);
        intens(obj,1)=mean(objvals);
        intens(obj,2)=median(objvals);
        intens(obj,3)=std(objvals);
        intens(obj,4)=sum(objvals);
        prcs=prctile(objvals,[percentile 100-percentile]);
        intens(obj,5)=prcs(1);
        intens(obj,6)=prcs(2);
        
        if erodelengthproportion
            erodew=round(erodewidth/100 * props(obj).MinorAxisLength);
            if erodew<0
                bigfill=padarray(zeros(size(mask)),[-erodew -erodew],0,'both');
            end
        else
            erodew=round(erodewidth);
        end
        
        %             borders=ones(size(fill));
        %             borders(1,:)=0; borders(:,1)=0; borders(size(borders,1),:)=0; borders(:,size(borders,2))=0;
        %             eroded=imerode(fill.*borders, strel('disk', round(erodew/2)));
        if erodew>=0
            fill=double(props(obj).Image);  % borders to zeros for erosion. one could ofcourse pad with zeros too...
            fill=padarray(fill,[erodew erodew],0,'both');
            eroded=imerode(fill, strel('disk', round(erodew/2)));
            fill=fill((erodew+1):(end-erodew),(erodew+1):(end-erodew));
            eroded=eroded((erodew+1):(end-erodew),(erodew+1):(end-erodew));
            centerimg=eroded.*gray(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            %centerimg=eroded.*gray;
            centermean=mean(centerimg(eroded>0));
            perimimg=(fill-eroded).*gray(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1);
            perimmean=mean(perimimg((fill-eroded)>0));
            perimmedian=median(perimimg((fill-eroded)>0));
            perimstd=std(perimimg((fill-eroded)>0));
            perimsum=sum(perimimg((fill-eroded)>0));
            imout(cds(2):cds(2)+cds(4)-1,cds(1):cds(1)+cds(3)-1)=imout(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1)+(bwperim(eroded)); % same as above butonly red channel
            fullmean=mean(objvals);
            fullmedian=median(objvals);
            fullstd=std(objvals);
            fullsum=sum(objvals);
        else
            % negative erosion becomes dilation
            fill=double(props(obj).Image);  % borders to zeros for erosion. one could ofcourse pad with zeros too...
            fill=padarray(fill,[-erodew -erodew],0,'both');
            dilated=imdilate(fill, strel('disk', round(-erodew/2)));
%            dilated=dilated.*imcomplement(fill);
            bigfilled=bigfill;
            bigfilled(cds(2):cds(2)+cds(4)-1-2*erodew,cds(1):cds(1)+cds(3)-1-2*erodew)=dilated;
            bigfilled=bigfilled((-erodew+1):(end+erodew),(-erodew+1):(end+erodew));
            bigfilled=bigfilled.*imcomplement(mask);
            %bigfill(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1)=double(props(obj).FilledImage);
            %eroded=imdilate((bigfill), strel('disk', round(-erodew/2)));
            %centerimg=bigfilled.*gray;
            centermean=intens(obj,1);
            %ringmask=imcomplement(mask).*(eroded-bigfill);
            %perimimg=bigfilled.*gray;
            perimmean=mean(gray(bigfilled==1));
            perimmedian=median(gray(bigfilled==1));
            perimstd=std(gray(bigfilled==1));
            perimsum=sum(gray(bigfilled==1));
            imout=imout+(bwperim(bigfilled));
            fullmean=mean([objvals; gray(bigfilled==1)]);
            fullmedian=median([objvals; gray(bigfilled==1)]);
            fullstd=std([objvals; gray(bigfilled==1)]);
            fullsum=sum([objvals; gray(bigfilled==1)]);
        end
        intens(obj,7)=perimmean;
        intens(obj,8)=perimmedian;
        intens(obj,9)=perimstd;
        intens(obj,10)=perimsum;
        intens(obj,11)=centermean/perimmean;
        intens(obj,12)=centermean-perimmean;
        intens(obj,13)=fullmean;
        intens(obj,14)=fullmedian;
        intens(obj,15)=fullstd;
        intens(obj,16)=fullsum;
        intens(obj,17:29)=CalculateHaralick(gray(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1),props(obj).Image,texturescale);
        intens(obj,30:33)=CalculateGabor(gray(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1),props(obj).Image,texturescale);
        intens(obj,34)=max(intens(obj,30:33));
    end
    
    dataout=[objnums intens];
else
   dataout=zeros(0,34);
end
dataout(isinf(dataout))=nan;
%imwrite(imout,[outdir filesep origimglist{file} '.png'])


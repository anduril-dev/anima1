function Gabor=CalculateGabor(OrigImage,mask,TextureScaleNum)

%%% Get Gabor features.
%%% The Gabor features are calculated by convolving the entire
%%% image with Gabor filters and then extracting the filter output
%%% value in the centroids of the objects in LabelMatrixImage
%  Edited for Anima purposes
% See original in https://svn.broadinstitute.org/CellProfiler/trunk/CellProfiler_old_matlab/Modules/MeasureTexture.m


MedianArea = size(OrigImage,1)*size(OrigImage,2);

sigma = sqrt(MedianArea/pi)/3;  % Set width of filter to a third of the median radius

% Use Gabor filters with three different frequencies
f = 1/(2*TextureScaleNum);

% Angle direction, filter along the x-axis and y-axis and 45 angles
theta = [0 pi/4 pi/2 3*pi/4];

KernelSize = max(size(OrigImage,1)/2,size(OrigImage,2)/2);

[x,y] = meshgrid(-KernelSize:KernelSize,-KernelSize:KernelSize);

% Apply Gabor filters and store filter outputs in the Centroid pixels
GaborFeatureNo = 1;
Gabor = zeros(1,length(theta));  % Initialize measurement matrix
m = 1;
for n = 1:length(theta)

    % Calculate Gabor filter kernel
    % Scale by 1000 to get measurements in a convenient range
    g = 1000*1/(2*pi*sigma^2)*exp(-(x.^2 + y.^2)/(2*sigma^2)).*exp(2*pi*sqrt(-1)*f(m)*(x*cos(theta(n))+y*sin(theta(n))));
    g = g - mean(g(:));           % Important that the filters has DC zero, otherwise they will be sensitive to the intensity of the image

    % Center the Gabor kernel over the centroid and calculate the filter response.

        % Cut patch
        p = OrigImage;

        if size(OrigImage,1) ~= size(g,1)
            p = [p;zeros(size(g,1)-size(p,1),size(p,2))];
        end

        if size(OrigImage,2) ~= size(g,2)
            p = [p zeros(size(p,1),size(g,2)-size(p,2))];
        end
        % Calculate the filter output
        filtered=g.*p;
        Gabor(1,GaborFeatureNo) = abs(sum(filtered(mask)));

    GaborFeatureNo = GaborFeatureNo + 1;
end


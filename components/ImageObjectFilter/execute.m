addpath('../../lib/matlab/')
try
    cf=readcommandfile(commandfile);  
    objectfilter(cf); 
catch me 
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  


function imagesegment(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

imdir=getinput(cf,'dir');
seeddir=getinput(cf,'seed');
colors=getinput(cf,'colors');
outdir=getoutput(cf,'mask');
outdirperim=getoutput(cf,'perimeter');
outdirnumber=getoutput(cf,'numbers');

createnumbered=getparameter(cf,'numbers','boolean');
%fillholes read every iteration
%clearborders read every iteration
correction=getparameter(cf,'corr','float');
constant=getparameter(cf,'constant','float');
mina=getparameter(cf,'minsize','float');
maxa=getparameter(cf,'maxsize','float');
minr=getparameter(cf,'minround','float');
maxr=getparameter(cf,'maxround','float');
mine=getparameter(cf,'minecc','float');
maxe=getparameter(cf,'maxecc','float');
minint=getparameter(cf,'minintensity','float');

method=lower(getparameter(cf,'method','string'));
  switch method
        case 'otsu'

        case 'shape'
        
        case 'constantarea'

        case 'knn'
            colordata=readcsvcell(colors,0,char(9));
            clusters=str2double(colordata.data(:,1));
            colorvalues=str2double(colordata.data(:,2:end));
        
        case 'kernel'
            colordata=readcsvcell(colors,0,char(9));
            clusters=str2double(colordata.data(:,1));
            colorvalues=str2double(colordata.data(:,2:end));
            kernel=getparameter(cf,'kernel','string');
        
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
            return
    end

imglist=getimagelist(imdir);
if strcmp(seeddir,'')
    seeddir=imdir;
end
seedlist=getimagelist(seeddir);
if (numel(imglist) ~= numel(seedlist))
    writeerror(cf,'Dir and Seed inputs contain different number of images.')
    return
end

mkdir(outdir);
mkdir(outdirperim);
mkdir(outdirnumber);

tic
for file=1:numel(imglist)
    fillholes=getparameter(cf,'fillholes','boolean');
    clearborders=getparameter(cf,'clearborders','boolean');
    id=[imdir filesep imglist{file}];
    seedid=[seeddir filesep seedlist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    gray=im2double(imread(id));
    seed=logical(imread(seedid));
    mask=false(size(gray));
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    switch method
        case 'otsu'
            mask=seededotsusegment(gray,seed,correction,minint);
            mask=logical(ridofwrongsize(mask,mina,maxa));
        case 'constantarea'
            mask=seededareasegment(gray,seed,constant);
            mask=logical(ridofwrongsize(mask,mina,maxa));
        case 'shape'
            mask=logical(seededshapesegment(gray,seed,mina,maxa,minr,maxr,mine,maxe,minint,fillholes,clearborders));
            fillholes=false;
            clearborderd=false;
        case 'knn'
            [mask,perim]=knnsegment(gray,colorvalues, clusters);
            fillholes=false;
            clearborders=false;
        case 'kernel'
            [mask,perim]=kernelsegment(gray,colorvalues, clusters,kernel);
            fillholes=false;
            clearborders=false;
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
    end
    if fillholes
        mask=imfill(mask,4,'holes');
    end
    if clearborders
        mask=imclearborder(mask,4);
    end
    
    if createnumbered
        cidx=mask2cidx(mask);
    end
    if ~exist('perim','var')
        perim=bwperim(mask,4);
    end
    imwrite(mask,[outdir filesep imglist{file} pngstr])
    imwrite(perim,[outdirperim filesep imglist{file}])
    clear perim
    if createnumbered
        imwrite(logical(mask_numbering(mask,cidx)),[outdirnumber filesep imglist{file} pngstr]);
    end
end
toc
end

function [textimage,d]=mask_numbering(mask,cidx)
    textimage=false(size(mask));
    [h w]=size(mask);
    for d=1:numel(cidx)
        [y,x]=ind2sub([h w],cidx{d}(1));
        textimage=max(textimage,writeonimage(textimage, mat2str(d), x-6, y-6));
    end
end

function cidx=mask2cidx(mask)
    labels=bwlabel(mask,4);
    cidx=cell(max(labels(:)),1);
    for d=1:max(labels(:))
        cidx{d}=(find(labels==d));
    end
end


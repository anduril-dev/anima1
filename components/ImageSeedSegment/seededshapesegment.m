function combmask=seededshapesegment(gray,seed,minarea,maxarea,minround,maxround,minecc,maxecc,minint,fillholes,clearborders)
% bwout=seededshapesegment(gray,seedmask,minarea,maxarea,minround,maxround,minint,fillholes,clearborders)
% GRAY is a gray scale image, SEEDMASK is a BW image


q=96;
props=regionprops(bwlabel(seed,4),gray,'Centroid','BoundingBox','Image','PixelValues','PixelIdxList');
objs=size(props,1);
combmask=false(size(gray));
if maxarea==0
    maxarea=1*numel(gray);
end
if objs>0
coords=reshape([props.Centroid], [2 objs])';
for obj=1:objs
    %cds=ceil(props(obj).BoundingBox);  % bounding box coordinates (x,y,w,h)
    tempmask=false(size(props(obj).Image));
    %tempmask=false(size(gray));
    %tempdata=zeros(size(gray));
    %tempdata(props(obj).PixelIdxList)=gray(props(obj).PixelIdxList);
    tempdata=zeros(size(tempmask));
    tempdata(props(obj).Image)=gray(props(obj).PixelIdxList);
    levels=flipud(unique(tempdata(tempdata>minint)));
    if numel(levels)>q
        levels=levels(round([1:q]*numel(levels)/q));
    end
    
    for leveliter=1:numel(levels)
        level=levels(leveliter);
        submask=or(tempmask,tempdata>level);
        if fillholes
            submask=imfill(submask,4,'holes');
        end
        sublabel=bwlabel(submask,4);
        subprops=regionprops(sublabel,'Area','Perimeter','Eccentricity');
        Areas=[subprops.Area];
        Round=sqrt(4*pi.*Areas./([subprops.Perimeter].^2));
        Ecc=[subprops.Eccentricity];
        goodareas=intersect(find(Areas>=minarea), find(Areas<=maxarea));
        goodround=intersect(find(Round<=maxround), find(Round>=minround));
        goodecc=intersect(find(Ecc<=maxecc), find(Ecc>=minecc));
        submask=ismember(sublabel,intersect(intersect(goodareas,goodround),goodecc));
        tempmask=or(submask,tempmask);
    end
    combmask(props(obj).PixelIdxList)=tempmask(props(obj).Image);
end    
    
% final round to make sure all objects are within ranges.
if fillholes
    combmask=imfill(combmask,'holes');
end
if clearborders
    combmask=imclearborder(combmask);
end
qlabel=bwlabel(combmask,4);
props=regionprops(qlabel,'Area','Perimeter');
Areas=[props.Area];
Round=sqrt(4*pi.*Areas./([props.Perimeter].^2));
goodareas=intersect(find(Areas>minarea), find(Areas<maxarea));
goodround=intersect(find(Round<maxround), find(Round>minround));
combmask=ismember(qlabel,intersect(goodareas,goodround));   
end

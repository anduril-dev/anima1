function mask=seededotsusegment(gray,seed,correction,minint)
% mask=seededotsusegment(gray,seed,correction)
% data is a gray scale image
% seed is a BW mask image
% correction is the value that multiplies graythresh
% minint is the minimum acceptable threshold value

props=regionprops(bwlabel(seed,4),gray,'Centroid','BoundingBox','Image','PixelValues');
objs=size(props,1);
mask=false(size(gray));
if objs>0
coords=reshape([props.Centroid], [2 objs])';
for obj=1:objs
    cds=ceil(props(obj).BoundingBox);  % bounding box coordinates (x,y,w,h)
    fill=double(props(obj).Image);
    tempmask=false(size(gray));
    tempmask(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1)=fill;
    level=graythresh(props(obj).PixelValues)*correction;
    level=max(minint,level);
    tempmask=logical(tempmask.*im2bw(gray,level));
    mask=mask | tempmask;
end
end

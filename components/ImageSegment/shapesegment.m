function combmask=shapesegment(data,minarea,maxarea,minround,maxround,minecc,maxecc,minint,fillholes,clearborders)
% bwout=shapesegment(data,minarea,maxarea,minround,maxround,minint,fillholes,clearborders)
% data is a gray scale image
% mins and maxes are the limits to find objects by shape
% fillholes and clearborder are booleans.

% Find how many intensity levels to examine ( max 96 )
q=96;
levels=flipud(unique(data(data>minint)));
if numel(levels)>q
    levels=levels(round([1:q]*numel(levels)/q));
end
if numel(levels)<4
    levels=linspace(1,minint,4);
end
% set maxarea to absolute square pixels if it was expressed as fraction
if maxarea==0
    maxarea=1*numel(data);
elseif maxarea<1
    maxarea=maxarea*numel(data);
end
% Iterate over intensity levels
combmask=zeros(size(data));
for leveliter=1:numel(levels)
    % threshold image, and combine with previous iteration
    level=levels(leveliter);
    qmask=or(combmask,data>level);
    if fillholes
        qmask=imfill(qmask,4,'holes');
    end
    if clearborders
        qmask=imclearborder(qmask,4);
    end
    qlabel=bwlabel(qmask,4);
    % Find object shapes, and filter out unwanted
    props=regionprops(qlabel,'Area','Perimeter','Eccentricity');
    Areas=[props.Area];
    Round=sqrt(4*pi.*Areas./([props.Perimeter].^2));
    Ecc=[props.Eccentricity];
    goodareas=intersect(find(Areas>=minarea), find(Areas<=maxarea));
    goodround=intersect(find(Round<=maxround), find(Round>=minround));
    goodecc=intersect(find(Ecc<=maxecc), find(Ecc>=minecc));
    
    qmask=ismember(qlabel,intersect(intersect(goodareas,goodround),goodecc));
    % save good objects for next iteration
    combmask=or(qmask,combmask);
end
% final round to make sure all objects are within ranges.
if fillholes
    combmask=imfill(combmask,'holes');
end
if clearborders
    combmask=imclearborder(combmask);
end
qlabel=bwlabel(combmask,4);
props=regionprops(qlabel,'Area','Perimeter');
Areas=[props.Area];
Round=sqrt(4*pi.*Areas./([props.Perimeter].^2));
goodareas=intersect(find(Areas>minarea), find(Areas<maxarea));
goodround=intersect(find(Round<maxround), find(Round>minround));
combmask=ismember(qlabel,intersect(goodareas,goodround));


function [detailImages,approximationImages] = ATrousWavelets(inImage,numberOfLevels)

% [detailImages,approximationImages] = ATrousWavelets(inImage,numberOfLevels)
% Outputs: the detail images and the approximation images
%
% This function decomposes the input image using a trous wavelets.
% "a trous" means "with holes".  This makes sense because we add zeros
% between the elements of the scaling function at each iteration.
% 
% Author: Dirk Padfield, GE Global Research, padfield@research.ge.com
%
% Reference: 
% Padfield, D., Rittscher, J., and Roysam, B., 
% "Coupled minimum-cost flow cell tracking for high-throughput quantitative analysis," 
% Medical Image Analysis 15(4), 650-668 (2011) 

% B3-spline scaling function.
% These are binomial filters, which are the integer special case of the
% Gaussian function.  They are found from the odd-length rows of
% Pascal's triangle to generate integer-coefficient Gaussian
% approximations.  An alternative would be to use the coefficients from the
% "The Magic Sigma" paper by Dirk Padfield.
% scalingFunction = 1/2^8 * [ ...
%     1     4     6     4     1;
%     4    16    24    16     4;
%     6    24    36    24     6;
%     4    16    24    16     4;
%     1     4     6     4     1];

% Using the "magic sigma" for a 5*5 kernel.
scalingFunction = [1 8 16 8 1]'*[1 8 16 8 1];
scalingFunction = 1/sum(scalingFunction(:)) * scalingFunction;
newScalingFunction = scalingFunction;

% The input image needs to have even dimensions so that its correlation
% with the scaling function (always odd) will be even.  This simplifies
% several of the operations including the symmetric padding.
% For each odd size dimension, the padSize will be 1.
initialPadSize = rem(size(inImage),2);
inImage = padarray(inImage, initialPadSize, 'symmetric', 'post');

% The imageSize will be the new even size image.
imageSize = size(inImage);
c(:,:,1) = inImage;

for level = 2:numberOfLevels
%    disp(['Level: ' num2str(level)]);
    % Since the structuring element roughly doubles in size at every
    % iteration, convolution can become very slow.  Therefore, we use
    % multiplication in the Fourier domain instead.
    % Using this method also gives better results on the boundaries.
    % Find the maximum image size: either the image or the newScalingFunction.
    if( sum(size(inImage) > size(newScalingFunction)) < 2 )
        error('Image size must be greater than structuring element size');
    end

    % Center the scalingFunction in an image of zeros the same size as the
    % correlation image.
    scalingFunctionBorderWidth = (imageSize)/2;
    paddedScalingFunctionImage = padarray(newScalingFunction, scalingFunctionBorderWidth, 0, 'pre');
    paddedScalingFunctionImage = padarray(paddedScalingFunctionImage, scalingFunctionBorderWidth-1, 0, 'post');

    % Center the previous approximation image in an image of zeros the same
    % size as the correlation size.
    borderWidth = (size(newScalingFunction)-1)/2;
    paddedApproximationImage = padarray(c(:,:,level-1), borderWidth, 'symmetric', 'both');
    
    correlationImage = real(ifft2(fft2(paddedApproximationImage).*fft2(fftshift(paddedScalingFunctionImage))));
    clear paddedApproximationImage paddedScalingFunctionImage;
    c(:,:,level) = correlationImage(1+borderWidth:end-borderWidth,1+borderWidth:end-borderWidth);
    clear correlationImage;
    
    % Augment the scaling function for the next level by inserting zeros
    % between each tap.
    prevScalingFunction = newScalingFunction;
    newScalingFunction = zeros(size(prevScalingFunction,1)*2-1,size(prevScalingFunction,2)*2-1);
    newScalingFunction(1:2:end,1:2:end) = prevScalingFunction;
end

approximationImages = zeros([size(inImage) numberOfLevels]);
detailImages = zeros([size(inImage) numberOfLevels-1]);

for level = 1:numberOfLevels
    % Remove the initial padding.
    approximationImages(:,:,level) = c(1:end-initialPadSize(1),1:end-initialPadSize(2),level);

    if( level > 1)
        % Create the wavelet images.
        detailImages(:,:,level-1) = approximationImages(:,:,level-1) - approximationImages(:,:,level);
    end
end

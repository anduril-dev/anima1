function imagesegment(cf)
%
% cf = Anduril command file
addpath('wavelet');
imdir=getinput(cf,'dir');
outdir=getoutput(cf,'mask');
outdirperim=getoutput(cf,'perimeter');
outdirnumber=getoutput(cf,'numbers');
outmasklist=getoutput(cf,'masklist');
createnumbered=getparameter(cf,'numbers','boolean');
%fillholes read every iteration
%clearborders read every iteration
correction=getparameter(cf,'corr','float');
constant=getparameter(cf,'constant','float');
mina=getparameter(cf,'minsize','float');
maxa=getparameter(cf,'maxsize','float');
minr=getparameter(cf,'minround','float');
maxr=getparameter(cf,'maxround','float');
mine=getparameter(cf,'minecc','float');
maxe=getparameter(cf,'maxecc','float');
minint=getparameter(cf,'minintensity','float');
domasklist=getparameter(cf,'masklist','boolean');
clusters=getparameter(cf,'clusters','float');

imglist=getimagelist(imdir);

method=lower(getparameter(cf,'method','string'));
  switch method
        case 'constant'

        case 'otsu'

        case 'shape'
        
        case 'constantarea'
        
        case 'kmeans'
        
        case 'snake'
            params=textscan(getparameter(cf,'snakeparam','string'),'%f',8,'Delimiter',',');
            params=params{1};
            seeddir=getinput(cf,'seed');
            if ~exist(seeddir,'dir')
                writeerror(cf,'You must give initial seed masks as a Masklist.')
                return
            end
        
        case 'wavelet'
            params=textscan(getparameter(cf,'waveletparam','string'),'%f',2,'Delimiter',',');
            params=params{1};
            disp(['min/max scales: ' num2str(params(1)) '/' num2str(params(2))]);
            bgdir=getinput(cf,'dir2');
        
        case 'correlation'
            if ~inputdefined(cf,'dir2')
                writeerror(cf,'You have to define input dir2 to use Correlation method')
                return
            end
            imdir2=getinput(cf,'dir2');
            imglist2=getimagelist(imdir2);
            if numel(imglist)~=numel(imglist2)
               writeerror(cf,'Image folders dir and dir2 contain different number of images')
               return
            end 
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
            return
    end

if clusters<2
    writeerror(cf,'Cluster number must be 2 or greater')
    return
end

mkdir(outdir);
mkdir(outdirperim);
mkdir(outdirnumber);
mkdir(outmasklist);
tic

for file=1:numel(imglist)
    fillholes=getparameter(cf,'fillholes','boolean');
    clearborders=getparameter(cf,'clearborders','boolean');
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    input=im2double(imread(id));
    mask=false(size(input));
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    switch method
        case 'constant'
            mask=im2bw(input,constant);
            mask=ridofwrongsize(mask,mina,maxa);
            mask=ridofwrongshape(mask,minr,maxr,mine,maxe);
        case 'otsu'
            level=min(graythresh(input)*correction,1);
            level=max(minint,level);
            mask=im2bw(input,level);
            mask=ridofwrongsize(mask,mina,maxa);
            mask=ridofwrongshape(mask,minr,maxr,mine,maxe);
        case 'shape'
            mask=shapesegment(input,mina,maxa,minr,maxr,mine,maxe,minint,fillholes,clearborders);
            fillholes=false;
            clearborderd=false;
        case 'constantarea'
            mask=areasegment(input,constant);
            mask=ridofwrongsize(mask,mina,maxa);
            mask=ridofwrongshape(mask,minr,maxr,mine,maxe);
        case 'kmeans'
            [mask,perim]=kmeanssegment(input,clusters,fillholes,clearborders,mina,maxa);
            fillholes=false;
            clearborderd=false;
        case 'snake'
            [mask,perim,cidx]=snakesegment(input,[seeddir filesep imglist{file}],params(1),params(2),...
                params(3),params(4),params(5),params(6),params(7),...
                params(8),mina,maxa);
            %crle=cellfun(@(x) maskrle(x),idxs,'UniformOutput',false);
            fillholes=false;
            clearborders=false;
            domasklist=true;
        case 'wavelet'
            % Input image has to be square, and the implementation sometimes fails at doing so
            orig_size=[size(input,1) size(input,2)];
            orig_max=max(orig_size);
            n=256;
            while n<orig_max
                n=n+256;
            end
            sq_input=zeros(n);
            sq_input(1:orig_size(1), 1:orig_size(2))=input;
            mask=ATrousWaveletBlobDetection(sq_input,params(1):params(2));
            mask=mask(1:orig_size(1), 1:orig_size(2));
            mask=ridofwrongsize(mask,mina,maxa);
            mask=ridofwrongshape(mask,minr,maxr,mine,maxe);
        case 'correlation'
            id2=[imdir2 filesep imglist2{file}];
            input2=im2double(imread(id2));
            if ~all(size(input)==size(input2))
                writeerror(cf,['Image sources ' id ' and ' id2 ' are of different dimensions']);
                return
            end
            mask=correlationsegment(input,input2,correction);
            mask=ridofwrongsize(mask,mina,maxa);
            mask=ridofwrongshape(mask,minr,maxr,mine,maxe);
        otherwise
            writeerror(cf,'Segmentation method was not recognized')
    end
    if fillholes
        mask=imfill(mask,4,'holes');
    end
    if clearborders
        mask=imclearborder(mask,4);
    end
    
    switch method
        case 'kmeans'
            imwrite(mask,[outdir filesep imglist{file} pngstr])
            %lvls=linspace(0,1,clusters);
            %perim=zeros(size(mask));
            %cidx={};
            %for c=2:clusters
            %    perim=perim+bwperim(mask>=lvls(c),4);
            %    cidx=[cidx; mask2cidx(mask>=lvls(c))];
            %end
            imwrite(perim, [outdirperim filesep imglist{file} pngstr])
        case 'snake'
            imwrite(logical(mask),[outdir filesep imglist{file} pngstr])
            imwrite(logical(perim),[outdirperim filesep imglist{file}])
        otherwise
            if domasklist || createnumbered
                cidx=mask2cidx(mask);
            end
            perim=bwperim(mask,4);
            imwrite(logical(mask),[outdir filesep imglist{file} pngstr])
            imwrite(logical(perim),[outdirperim filesep imglist{file}])
    end
    if createnumbered
        imwrite(logical(mask_numbering(mask,cidx)),[outdirnumber filesep imglist{file} pngstr]);
    else
        %imwrite(false(size(mask)),[outdirnumber filesep imglist{file} pngstr]);
    end
    if domasklist
        if numel(cidx)==0
           cidx{1}=[];
        end
        mkdir([outmasklist filesep imglist{file} pngstr]);
        masksize=false(size(mask));
        for o=1:numel(cidx)
            idxobjwrite(cidx{o},masksize,[outmasklist filesep imglist{file} pngstr filesep num2str(o) '.png' ]);
        end
        %cellfun(@(x) dlmwrite([outtable filesep imglist{file} '.txt'],x,...
        %    'delimiter','\t','precision','%d','-append'),crle)
        
    end
end
toc
end

function [textimage,d]=mask_numbering(mask,cidx)
    textimage=false(size(mask));
    [h w]=size(mask);
    for d=1:numel(cidx)
        [y,x]=ind2sub([h w],cidx{d}(1));
        textimage=max(textimage,writeonimage(textimage, mat2str(d), x-6, y-6));
    end
end

function cidx=mask2cidx(mask)
    labels=bwlabel(mask,4);
    cidx=cell(max(labels(:)),1);
    for d=1:max(labels(:))
        cidx{d}=(find(labels==d));
    end
end
function idxobjwrite(idx,obj,fname)
    obj(idx)=true;
    imwrite(obj,fname);
end


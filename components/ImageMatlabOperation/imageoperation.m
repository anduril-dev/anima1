function imageoperation(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

for t=1:5
    imdir{t}=getinput(cf,['dir' num2str(t)]);
    imglist{t}=getimagelist(imdir{t});
end
imgalist={};
if inputdefined(cf,'array')
    arraytable=readarray(cf,'array');
    arraydirs=getcellcol(arraytable,'File');
    for t=1:numel(arraydirs)
        imadir{t}=arraydirs{t};
        imgalist{t}=getimagelist(imadir{t});
    end
end
for t=1:2
    filedir{t}=getinput(cf,['file' num2str(t)]);
    filelist{t}=getimagelist(filedir{t});
end
srclist=imglist{1};
% If dir1 is not defined, and array has inputs, then use array(1) for naming of files.
if ~inputdefined(cf,'dir1')
    if inputdefined(cf,'array')
       srclist=imgalist{1};
    end
end

tablefile=getinput(cf,'table');
outdir=getoutput(cf,'dir');
outdir2=getoutput(cf,'dir2');
tabledir=getoutput(cf,'table');

ext=getparameter(cf,'extension','string');
for t=1:10
    eval(sprintf('param%d = getparameter(cf,''param%d'',''guess'');',t,t));
end
files=getparameter(cf,'files','float');
% script may be multirow, use sprintf:
script=sprintf(getparameter(cf,'script','string'));
if  numel(script)==0
    scriptfile=getinput(cf,'script');
    fid=fopen(scriptfile,'rt');
    if fid~=-1
        script=textscan(fid,'%s','Delimiter','','EndOfLine','');
        fclose(fid);
        script=char(script{1});
    else
        script=' ';
        writeerror(cf,'No script to run');
        return
    end
end
mkdir(outdir);
mkdir(outdir2);
mkdir(tabledir);

if numel(srclist)==0
    numberpad=num2str(numel(num2str(files)));
    srclist=cellfun(@(x) ['file', x],...
        arrayfun(@(x) num2str(x,['%0' numberpad 'd']),1:files,'UniformOutput',false)',...
        'UniformOutput',false);
    if (strcmp(ext,''))
        writeerror(cf,'You must give file extension when running without source files (e.g. ".png")');
        return
    end
end
% check that dir inputs have same number of images
for t=2:5
    if numel(imglist{t})>0
        if numel(srclist)~=numel(imglist{t})
            writeerror(cf,['Source directory file numbers do not match (1 vs. dir' num2str(t) ')']);
            return
        end
    end
end
% check that array dir inputs have same number of images
for t=1:numel(imgalist)
    if numel(imgalist{t})>0
        if numel(srclist)~=numel(imgalist{t})
            writeerror(cf,['Source directory file numbers do not match (1 vs. array' num2str(t) ')']);
            return
        end
    end
end

if inputdefined(cf,'table')
    table=readcsvcell(tablefile,0,char(9));
end
disp(script)

for t=1:2
    if numel(filelist{t})>0
        fileid{t}=[filedir{t} filesep filelist{t}{1}];
        disp(['Read image: ' filelist{t}{1}]);
        eval(sprintf('file%d = im2double(imread(fileid{t}));', t));
    end
end
for file=1:numel(srclist)
    disp(['Cycle: ' num2str(file) '/' num2str(numel(srclist)) ]);
    for t=1:5
        if numel(imglist{t})>0
            id{t}=[imdir{t} filesep imglist{t}{file}];
            disp(['Read image: ' imglist{t}{file}]);
            eval(sprintf('im%d = im2double(imread(id{t}));', t));
        end
    end
    for t=1:numel(imgalist)
        if numel(imgalist{t})>0
            ida{t}=[imadir{t} filesep imgalist{t}{file}];
            disp(['Read image: ' imgalist{t}{file}]);
            eval(sprintf('ima%d = im2double(imread(ida{t}));', t));
        end
    end
    clear imout imout2 tableout;
    eval(script)
    if exist('imout','var')
    if numel(imout)>0
        disp(['Write image 1: ' srclist{file} ext])
        imwrite(imout,[outdir filesep srclist{file} ext]);
    end
    end
    if exist('imout2','var')
    if numel(imout2)>0
        disp(['Write image 2: ' srclist{file} ext])
        imwrite(imout2,[outdir2 filesep srclist{file} ext]);
    end
    end
    if exist('tableout','var')
        disp(['Write table: ' srclist{file} '.csv'])
        writefilecsv([tabledir filesep srclist{file} '.csv'],tableout);
    end
    end
end

<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>ImageLocalMaxima</name>
    <version>1.2</version>
    <doc>Finds local maxima centers in a grayscale intensity image. The component uses algorithms implemented by John C. Crocker, David G. Grier and Eric R. Dufresne.
    
     Example:
      <table><tr><th>Source</th><th>Output</th></tr>
      <tr><td><img src="input.jpg"/></td><td><img src="output.png"/></td><td>
<pre>
"File"      "X" "Y" "Brightness" "Gyration radius2"
"cells.tif" 59  90      44          0.05
"cells.tif" 67 128      70          0.07  ...
</pre>
      
      </td></tr>
      </table>
    </doc>
    <author email="ville.rantanen@helsinki.fi">Ville Rantanen</author>
    <category>Image Analysis</category>
    <launcher type="matlab">
        <argument name="file" value="execute.m" />
        <argument name="source" value="pkfnd.m" />
        <argument name="source" value="imagelocalmax.m" />
        <argument name="source" value="cntrd.m" />        
    </launcher>
    <requires>Matlab</requires>
    <inputs>
        <input name="dir" type="ImageList"><doc>A directory with grayscale image files.</doc>
        </input>
        <input name="origdir" type="ImageList" optional="true"><doc>A directory with the original image files, for naming the centers. If omitted, names from "dir" are used.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="mask" type="ImageList"><doc>BW PNG images with centers marked.</doc>
        </output>
        <output name="centerlist" type="CSV"><doc>List of center points.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="lowpass" type="int" default="9"><doc>Lowpass filter size. Must be odd. Use 0 for disabling.</doc> </parameter>
        <parameter name="lowint" type="float" default="0.05"><doc>Lowest intensity to accept as a maximum. Range 0 to 1.</doc> </parameter>
        <parameter name="maximasize" type="int" default="11"><doc>Radius of maximum to search. Must be odd.</doc> </parameter>
        <parameter name="centersize" type="int" default="11"><doc>Radius of center search area. Must be odd.</doc> </parameter>
    </parameters>
</component>


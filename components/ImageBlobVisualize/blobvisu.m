function blobvisu(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

imdir=getinput(cf,'dir');
outdir=getoutput(cf,'visu');
csvfile=getinput(cf,'objects');
imorigdir=getinput(cf,'origdir');
clustcolname=getparameter(cf,'clusterCol','string');


nobg=getparameter(cf,'nobg','boolean');
erodewidth=getparameter(cf,'erodewidth','float');
erodelengthproportion=getparameter(cf,'isratio','boolean');
tic
imglist=getimagelist(imdir);
if numel(imorigdir)<2
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Image name list and gray scale list numbers do not match');
    disp('Image name list and gray scale list numbers do not match');
    return;
end

blobdata=readcsvcell(csvfile);
data.object=getcellcol(blobdata,'Object','float');
data.file=getcellcol(blobdata,'File','string');
data.alpha=getcellcol(blobdata,'Alpha','float');
data.lx=getcellcol(blobdata,'Lx','float');
data.ly=getcellcol(blobdata,'Ly','float');
data.cy=getcellcol(blobdata,'Y','float');
data.cx=getcellcol(blobdata,'X','float');
if numel(clustcolname)>0
    data.cluster=getcellcol(blobdata,clustcolname,'string');
    [foosort, sortidx]=sort(data.object);
    data=structfun(@(x) x(sortidx),data,'UniformOutput',false);

%% cluster coloring
    uniqclust=unique(data.cluster)';
    writelog(cf,[num2str(numel(uniqclust)) ' distinct cluster ids'])
    remap=0;
    if isnumeric(uniqclust)
        if all(uniqclust==1:numel(uniqclust))
            remap=0;
        else
            remap=1;
        end
    else
        remap=1;
    end
    if remap
        clusters=zeros(size(data.cluster));
        if isnumeric(uniqclust)
            for uc=1:numel(uniqclust)
                clusters(data.cluster==uniqclust(uc))=uc;
            end
        else
            for uc=1:numel(uniqclust)
                clusters(strcmp(data.cluster,uniqclust(uc)))=uc;
            end
        end
        data.cluster=clusters;
        clear clusters;
    end
    
    hsvmap=linspace(0,1,numel(uniqclust)+1)';
    hsvmap(:,2:3)=1;
    hsvmap=hsvmap(1:(end-1),:); % remove the last item not to do full circle.
    cmap=hsv2rgb(hsvmap); 
    cmap2=[cmap; 0 0 0];
end

clear blobdata;
%% image looping

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    im=im2double(imread(id));
    if ndims(im)<3
        im=repmat(im,[1 1 3]);
    end
    if nobg
        im=zeros(size(im));
    end
    maskim=zeros(size(im));
    rows=strcmp(origimglist{file},data.file);
        
    alpha=data.alpha(rows);
    lx=data.lx(rows);
    ly=data.ly(rows);
    cy=data.cy(rows);
    cx=data.cx(rows);
        
    ellipseprec=max(32,round(8*max([lx;ly])));
    
       %[data.cx data.cy data.Xbar data.Ybar data.alpha data.lx data.ly] = BlobDetect(im,0.01,15,50,true,0.005,10,true); 
    [height,width,foo] = size(maskim);
    [Xbar,Ybar]=getedges(cx,cy,alpha,lx,ly,ellipseprec,width,height);
    if numel(clustcolname)>0
        objects=data.object(rows);
        clusters=data.cluster(rows);
        tempim=zeros([height width]);
        for obj=1:numel(objects)
            points=sub2ind([height width],(Ybar(obj,:)),(Xbar(obj,:)));
            tempim(points)=clusters(obj);
        end
        labelclass=uint8(tempim);
        labelsout=im2double(label2rgb(labelclass,(cmap),'k'));
        classlocations=repmat(labelclass>0,[1 1 3]);
        labelsout(classlocations==0)=im(classlocations==0);
        maskim=im2double(labelsout);
    else
        points=sub2ind([height width],(Ybar(:)),(Xbar(:)));
        maskim(points)=1;
        maskim=repmat(maskim(:,:,1),[1 1 3]);  % this is a bit extra step but must be done to be compatible with clustervisu
        maskim=im+maskim;
    end
    
    if erodewidth~=0
        if erodelengthproportion
            erodew=(erodewidth/100).*min(lx,ly);
        else
            erodew=erodewidth;
        end
        [Xbarer,Ybarer]=getedges(cx,cy,alpha,lx-erodew,ly-erodew,ellipseprec,width,height);
        pointser=sub2ind([height width],(Ybarer(:)),(Xbarer(:)));
        tempim=zeros([height width]);
        tempim(pointser)=1;
        maskim=maskim+repmat(tempim(:,:,1),[1 1 3]);
    end % no erosion vis.
    
    if ~exist(outdir,'dir'), mkdir(outdir); end

    imwrite(maskim,[outdir filesep imglist{file}]);

    resultlist=getimagelist(outdir);
    fprintf('%d of %d done.\n',numel(resultlist),numel(imglist))
end
toc
end

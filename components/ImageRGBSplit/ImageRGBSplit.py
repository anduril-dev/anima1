#!/usr/bin/python
import sys
import anduril.imagetools as ait
from anduril.args import *
import re
import os
import scipy.misc

out={}
out[0] = r
out[1] = g
out[2] = b
for c in range(0,3):
    os.mkdir(out[c])

# read list
if (rgb != None):
    imagelist=ait.get_imagelist(rgb)
else:
    imagelist=[]

# read images and extract channel c, then write.
i=1
for l in imagelist:
    print(l+': '+str(i)+'/'+str(len(imagelist)))
    im=ait.load_image(os.path.join(rgb,l),flatten=0)
    
    if im.ndim!=3:
        write_error(l+' is not RGB color image')
        sys.exit(1)
    ispng=re.search('png$',l.lower())
    if ispng:
        pngstr=''
    else:
        pngstr='.png'
    for c in range(0,3):
        im_channel=(im[:,:,c]*255).astype('uint8')
        #imtosave=scipy.misc.toimage(im_channel,cmin=0,cmax=1)
        #imtosave.save(out[c]+os.sep+l+pngstr)
        ait.save_png(out[c]+os.sep+l+pngstr,
                im_channel,grayscale=True,bitdepth=8)




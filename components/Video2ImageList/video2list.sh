
source "$ANDURIL_HOME/bash/functions.sh"
export_command

echo $format 

AVCONV=$( which avconv 2> /dev/null )
if ( "$AVCONV" -version 2>&1 | grep -qi avconv ) 2>&1 > /dev/null
then echo "avconv found."
else 
    AVCONV=$( which ffmpeg 2> /dev/null )
    if ( "$AVCONV" -version 2>&1 | grep -qi FFmpeg ) 2>&1 > /dev/null
    then echo "FFmpeg found."
    else
        msg=$AVCONV" not found. Maybe it is not installed on this system?"
        echo $msg >&2
        echo $msg >> "$errorfile"
        echo $slsep >> "$errorfile"
        exit 1
    fi
fi

mkdir "$output_list"
cd "$output_list"

"$AVCONV" -i "$input_video" -an -f image2 "${parameter_format}" >> "$logfile" 2>> "$logfile"
echo $slsep >> "$logfile"

if [ $( ls | wc -l ) -eq 0 ]
then echo error converting file  >> "$errorfile"
    exit 1
fi
exit 0




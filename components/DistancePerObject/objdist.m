function objdist(cf)
%
% cf = Anduril command file
tic

reffile=getinput(cf,'ref');
datafile=getinput(cf,'data');
outfile=getoutput(cf,'dist');
sumoutfile=getoutput(cf,'summary');

refxname=getparameter(cf,'refXCol','string');
refyname=getparameter(cf,'refYCol','string');
reffname=getparameter(cf,'refFCol','string');
xname=getparameter(cf,'XCol','string');
yname=getparameter(cf,'YCol','string');
fname=getparameter(cf,'FCol','string');

toofar=getparameter(cf,'toofar','float');
if toofar==0
    toofar=realmax;
end
refs=readcsvcell(reffile,0,char(9));
data=readcsvcell(datafile,0,char(9));

try
refx=getcellcol(refs,refxname,'float');
refy=getcellcol(refs,refyname,'float');
reff=getcellcol(refs,reffname,'string');
x=getcellcol(data,xname,'float');
y=getcellcol(data,yname,'float');
f=getcellcol(data,fname,'string');
catch ME
    writeerror(cf,'Problem with finding data columns');
end

%clear refs data
uniqreff=unique(reff);
outrow=1;
numout=zeros(min(numel(x),numel(refx)),4);
sumout=zeros(numel(uniqreff),5);
for file=1:numel(uniqreff)
    reffilerows=strcmp(uniqreff{file},reff);
    datafilerows=strcmp(uniqreff{file},f);
    dx=x(datafilerows);
    dy=y(datafilerows);
    fx=refx(reffilerows);
    fy=refy(reffilerows);
    objnum=1;
    while and(numel(fx)>0,numel(dx)>0) %% we do this to always measure the best matches first
        [dists,distidx]=pdist2([fx fy],[dx dy],'euclidean','smallest',1);
        [mins,minidx]=min(dists);
        numout(outrow,1)=objnum;
        numout(outrow,2)=fx(distidx(minidx));
        numout(outrow,3)=fy(distidx(minidx));
        numout(outrow,4)=mins;
        numout(outrow,5)=dx((minidx));
        numout(outrow,6)=dy((minidx));
        numout(outrow,7)=mean([numout(outrow,2) numout(outrow,5)]);
        numout(outrow,8)=mean([numout(outrow,3) numout(outrow,6)]);
        strout{outrow,1}=uniqreff{file};
        objnum=objnum+1;
        outrow=outrow+1;
        fx(distidx(minidx))=[];
        fy(distidx(minidx))=[];
        dx((minidx))=[];
        dy((minidx))=[];
    end
    sumstrout{file,1}=uniqreff{file};
    values=numout(outrow-objnum+1 : outrow-1,4);
    values=values(values<toofar);
    sumout(file,5)=mean(values);
    sumout(file,1)=sum(numout(outrow-objnum+1 : outrow-1,4)<toofar); %matching objects
    sumout(file,2)=sum(numout(outrow-objnum+1 : outrow-1,4)>=toofar); % matching but too far
    sumout(file,3)=objnum; % index at the remaining references
    while numel(fx)>0 %% we do this to always measure the best matches first
        numout(outrow,1)=objnum;
        numout(outrow,2)=fx(1);
        numout(outrow,3)=fy(1);
        numout(outrow,4)=nan;
        numout(outrow,5)=nan;
        numout(outrow,6)=nan;
        numout(outrow,7)=nan;
        numout(outrow,8)=nan;
        strout{outrow,1}=uniqreff{file};
        objnum=objnum+1;
        outrow=outrow+1;
        fx(1)=[];
        fy(1)=[];
    end
    sumout(file,3)=objnum-sumout(file,3); % reference points missed
    sumout(file,4)=objnum; % index before clearing extra objects
    while numel(dx)>0 %% we do this to always measure the best matches first
        numout(outrow,1)=objnum;
        numout(outrow,2)=nan;
        numout(outrow,3)=nan;
        numout(outrow,4)=nan;
        numout(outrow,5)=dx(1);
        numout(outrow,6)=dy(1);
        numout(outrow,7)=nan;
        numout(outrow,8)=nan;

        strout{outrow,1}=uniqreff{file};
        objnum=objnum+1;
        outrow=outrow+1;
        dx(1)=[];
        dy(1)=[];
    end
    sumout(file,4)=objnum-sumout(file,4); % number of objects probably extra
end

isnearidx=numout(:,4)<toofar;
%toofaridx=numout(:,4)>toofar;
numout(:,9)=isnearidx;

out.columnheads={'File','Object','Ref cx','Ref cy','Distance','Data cx','Data cy','Mean cx','Mean cy','IsPaired'};
outcell=num2cell(numout);
out.data=cellfun(@(x) sprintf('%g',x), outcell,'UniformOutput',false);
out.data=[strout out.data];
writefilecsv(outfile,out);

out.columnheads={'File','Matched','Too far','Missed','Extra','Mean of distances'};
outcell=num2cell(sumout);
out.data=cellfun(@(x) sprintf('%g',x), outcell,'UniformOutput',false);
out.data=[sumstrout out.data];
writefilecsv(sumoutfile,out);

toc


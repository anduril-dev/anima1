
source "$ANDURIL_HOME/bash/functions.sh"
BFPATH=$( readlink -f ../../lib/bftools )
export PATH=$PATH:$BFPATH

echo '         Welcome to Anima
       ▀██████░███░▄          
        ▀██████░███░██████▄         
         ███████░██░█████░░▄       
          ███████░░░███░░████      
          ▀█████████░█░██████      
           ██████████░███████      
           ███████░░█░████▀        
           ▀████░░████░██▀         
            ▀██░███████░▀▄         
                 ▀▀▀▀   ▀█         
                          ▀▄       
                            █      
         ▄▀▄  █▄ █ █ █▄ ▄█  ▄▀▄   ▄▀   
        █ ▀▀█ █ ▀█ █ █ ▀ █ █▀▀ █ █   
                                █  
                              ▄▀    
                             █     
                             █     
                            ▀  
'

# make the list separator a newline
IFS=$'\n' 

indir=$( getinput dir )
outdir=$( getoutput channel )

BF="bfconvert"

switches=$( getparameter switches ) 
infilter=$( getparameter filter )
namestring=$( getparameter name )
toPNG=$( getparameter pngOutput )
forceGray=$( getparameter gray )
multiplier=$( getparameter multiplier )
iscmd $BF || exit 1

# list files
if [ -d "$indir" ]
then infiles=( $( ls -1 "$indir" | grep -i "$infilter" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${outdir}" || writeerror "error creating directory ${outdir}"

# loop over images
for (( f=0; f<${#infiles[@]}; f++ ))
    do writelog "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    newbase="${infiles[$f]%.*}"
    newname=$( echo $namestring | sed -e "s,%o,${newbase},g" -e "s,$,.tif," )
    eval "$BF" -nooverwrite $switches \"${indir}/${infiles[$f]}\" \"${outdir}\"/\"${newname}\"

done

cd "${outdir}"
if [ $forceGray = "true" ]
then writelog "Forcing grayscale"
     iscmd "convert" || exit 1
     for f in $( ls )
     do convert "$f" -verbose -separate \
          -background black -compose plus -flatten "$f".miff
        convert "$f".miff "$f"
        rm "$f".miff
     done
fi

if [ $( echo "$multiplier==1.0" | bc ) -eq 0 ]
then writelog "Multiplying"
     iscmd "mogrify" || exit 1
     for f in $( ls )
     do mogrify -verbose -evaluate multiply $multiplier "$f" 
     done
fi

# convert to PNG if requested
if [ $toPNG = "true" ]
then writelog "Converting to PNG"
    cd "${outdir}"
    iscmd "convert" || exit 1
    for f in $( ls )
    do convert "$f" -verbose -depth 16 "${f%tif}png"
       [[ -e "${f%tif}png" ]] || {
           writeerror "could not convert file $f"
           exit 1
       }
       rm "$f"
    done
fi

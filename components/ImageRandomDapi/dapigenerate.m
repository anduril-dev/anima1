function dapigenerate(cf)
tic
writeto=getoutput(cf,'gray');
coordsfile=getoutput(cf,'table');
testing=getparameter(cf,'test','boolean');

w=getparameter(cf,'w','float');
h=getparameter(cf,'h','float');
mkdir(writeto)
no=getparameter(cf,'cellmax','float');
mindist=getparameter(cf,'mindist','float');
cellimgpath=regexprep(mfilename('fullpath'),'dapigenerate$','cellimages');
cellimgs=getimagelist(cellimgpath);
celimgs=numel(cellimgs);  disp(['Found ' num2str(celimgs) ' cells to randomize from.']);
imtowrite=getparameter(cf,'number','float');

out.columnheads={'File','Object','X','Y'};

for imn=1:imtowrite
    n=no; %floor(rand(1)*20+40);
    im=zeros(h,w);
    iter=1;
    if testing
        cxs=floor(linspace(0,w-50,n)'); cxsnew=cxs;
        cys=floor(linspace(0,h-50,n)'); cysnew=cys;
        cinds=ones(n,1);
        ca=zeros(n,1);
    else
        % change the seed
        rng('shuffle');
        cxs=floor(rand(n,1)*(w-50)); cxsnew=cxs;
        cys=floor(rand(n,1)*(h-50)); cysnew=cys;
        cinds=1+floor(rand(n,1)*(celimgs));
        ca=(rand(n,1)*2*pi);
    end
    ci=cell(n,1); ct=cell(n,1);
    for c=1:n
        ci{c}=im2double(imread([cellimgpath filesep cellimgs{cinds(c)} ]));
        ci{c}=ci{c}-min(ci{c}(:));
        ccross=im2bw(ci{c},graythresh(ci{c}));
        ccross=imdilate(ccross,ones(5));
        ccross=imfilter(double(ccross),ones(10)/100,'replicate');
        ci{c}=ci{c}.*ccross;
        xform=[cos(ca(c)) sin(ca(c)) 0; -sin(ca(c)) cos(ca(c)) 0; 0 0 1 ];
        T=maketform('affine',xform);
        ct{c}=imtransform(ci{c},T);
        cysnew(c)=cys(c)+round(size(ct{c},1)/2); 
        cxsnew(c)=cxs(c)+round(size(ct{c},2)/2);
    end
    while true
        d=pdist([cxsnew cysnew]);
        mindistnow=min(d);
        if mindistnow > mindist
            break
        end
        [locx, locy]=find(squareform(d)==mindistnow);
        cxsnew(locx(1))=[];
        cysnew(locx(1))=[];
        cxs(locx(1))=[];
        cys(locx(1))=[];
        ct(locx(1))=[];
        iter=iter+1;
    end
    n=numel(cxs);
    disp(['Removed ' num2str(no-n) ' objects from ' num2str(no)]);
    disp(['Creating image'  ' (' num2str(imn) '/' num2str(imtowrite) ')'])
    for c=1:n
        xform=[1 0 0; 0 1 0; cxs(c) cys(c) 1 ];
        T=maketform('affine',xform);
        c2=imtransform(ct{c},T, 'XData', [1 size(ct{c},2)+cxs(c)], ...
            'YData', [1 size(ct{c},1)+cys(c)],'XYScale',1, ...
            'Size', [h w]);
        im=max(im,c2);
%         im(cysnew(c),cxsnew(c))=1;
    end
    disp('Background generation');
    im2=im;
    imover=im(im>0.7)-0.7;
    imover=0.7+0.3*imover/max(imover);
    im2(im>0.7)=imover; %im(im>0.7)/max(im(:));
    im2=im2+0.05;
    bgsize=floor(max(h,w)/3);
    imf=(0.08*mat2gray((imfilter(im2,fspecial('gaussian',[bgsize bgsize],80)))));
    if testing
        fl=0;
    else
        fl=floor(rand(1)*3);
    end
    switch fl
        case 0
            imf=flipud(imf);
        case 1
            imf=fliplr(imf);
        case 2
            imf=rot90(imf,2);
    end
    im=im2+imf;
    
    imwrite(im,[writeto filesep num2str(imn) '_' num2str(n) '.png']);
    
    textdata=repmat({[num2str(imn) '_' num2str(n) '.png']},[n,1]);
    numdata=num2csvcell([(1:n)' cxsnew cysnew]);
    out.data=[textdata numdata];
    appendcsv(coordsfile,out);
    
end

toc

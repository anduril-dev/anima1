function [dataout,skelimg]=imagesinglemorph(id,nth,bramin,masklist,connectivity,fillsmaller)

mask=im2double(imread(id));
if size(mask,3)~=1
    error('Anduril:ImageTypeError','Source file not gray scale')
    return
end
if exist(masklist,'dir')
    maskimages=getimagelist(masklist);
else
    maskimages=false;
end
if iscell(maskimages)
    disp(['Using MaskList with ' num2str(numel(maskimages)) ' objects']);
    props=struct('Area',{},'Centroid',{},'BoundingBox',{},'MajorAxisLength',{},'MinorAxisLength',{},...
        'Eccentricity',{},'Orientation',{},'Image',{},'FilledArea',{},'Solidity',{},'Perimeter',{});
    for s=1:numel(maskimages)
        props(s,1)=regionprops(imread([masklist filesep num2str(s) '.png']),'Area','FilledArea','Perimeter','Image','Centroid','Eccentricity','BoundingBox','Orientation','MajorAxisLength','MinorAxisLength','Solidity');
    end
else
    props=regionprops(bwlabel(mask,connectivity),'Area','Perimeter','Image','Centroid','Eccentricity','BoundingBox','Orientation','MajorAxisLength','MinorAxisLength','Solidity','FilledArea');
end
objs=size(props,1);

coords=reshape([props.Centroid], [2 objs])';
dists=dist(coords');
sorted=sort(dists,1);
if objs > 1+nth
    dists=sorted(nth+1,:)';
elseif objs > 0
    dists=sorted(end,:)';
else
    dists=[];
end
skelimg=zeros(size(mask));
dataout=zeros(0,20);
if objs>0
    objnums=(1:objs)';
    areas=[props.Area]';
    filledareas=[props.FilledArea]';
    perimeters=[props.Perimeter]';
    roundness=sqrt(4*pi*areas./(perimeters.^2));
    lengthestimate=perimeters./(2+(pi-2).*roundness.^3);
    eccentricities=[props.Eccentricity]';
    orientations=[props.Orientation]';
    solidity=[props.Solidity]';
    skelwidth=zeros(objs,1);
    skelstd=zeros(objs,1);
    skellength=zeros(objs,1);
    skelbranch=zeros(objs,1);
    skelends=zeros(objs,1);
    extremalength=zeros(objs,1);
    for o=1:objs
        cds=ceil(props(o).BoundingBox);  % bounding box coordinates (x,y,w,h)
        objimg=double(props(o).Image);
        % Object-wise operations, mainly the skeleton related.. 
        %skeletonimg = bwmorph(props(o).FilledImage,'skel',Inf);
        %skeletonimg = bwmorph(skeletonimg,'spur',Inf);
        skeletonimg = bwmorph(objimg,'thin',Inf);
        %origskeleton=skeletonimg;
        cntx=props(o).Centroid(1);
        cnty=props(o).Centroid(2);
        if sum(skeletonimg(:))==0
            skeletonimg(cnty,cntx)=1;
        end
        if fillsmaller>0
            % Fill small holes in the skeleton
            %Thanks: Steve on image processing...
            filled = imfill(skeletonimg, 'holes');
            holes = filled & ~skeletonimg;
            bigholes = bwareaopen(holes, fillsmaller,4);
            smallholes = holes & ~bigholes;
            skeletonimg = bwmorph((skeletonimg | smallholes),'thin',Inf);
        end

        if bramin>0
            % get rid of branchpoints vs endpoints and other branchpoints that are too near (less than bramin)
            skeletonimg=skeletonclean(skeletonimg,bramin);
        end
        endimg=im2double(bwmorph(skeletonimg,'endpoints'));
        braimg=im2double(bwmorph(skeletonimg,'branchpoints'));        
        
        [skelwidth(o),skelstd(o)]=avgskeletonwidth(bwperim(objimg),skeletonimg);
        skellength(o)=sum(skeletonimg(:));
        extremalength(o)=extremaborderlength(objimg,cntx,cnty);
        
        skelends(o)=sum(endimg(:)==1);
        skelbranch(o)=sum(braimg(:)==1);
        skeletonimg=im2double(skeletonimg).*0.75;%+im2double(origskeleton)*0.25;
        skeletonimg(braimg>0)=braimg(braimg>0)*0.5;
        skeletonimg(endimg>0)=endimg(endimg>0);
        
        % draw the skeleton on a sum image containing all objects:
        skelimg(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1)=skelimg(cds(2):cds(2)+cds(4)-1, cds(1):cds(1)+cds(3)-1)+skeletonimg;
        
    end
    skelwidth(skelwidth==0)=1;
    dataout=[objnums areas filledareas perimeters roundness solidity eccentricities ...
        [props.MinorAxisLength]'/2 [props.MajorAxisLength]'/2 pi*orientations/180 ...
        skelwidth skelstd skellength skelbranch skelends extremalength lengthestimate ...
        dists coords(:,1) coords(:,2)];
end
% remove Infs (typically single pixel's roundness)
dataout(isinf(dataout))=nan;



function [length,width]=extremaborderlength(mask,cntx,cnty);
% function [length,width]=extremaborderlength(mask,cntx,cnty);
% Calculates the extrema length of an object by finding the longest distance between mask perimeter pixels.

if nargin<2
    [cnty,cntx]=find(mask==1);
    cntx=round(mean(cntx)); cnty=round(mean(cnty));
end
perim=bwperim(mask,4);

[y,x]=find(perim==1);
dists=(pdist([y x],'euclidean'));
length=max(dists(:));
if isempty(length)
    length=1;
end



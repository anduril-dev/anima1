function imagemorph(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file

tic
imdir=getinput(cf,'mask');
imdirlist=getinput(cf,'masklist');
imorigdir=getinput(cf,'origdir');

outfile=getoutput(cf,'table');
outdir=getoutput(cf,'skeleton');
nth=getparameter(cf,'nthdist','float');
bramin=getparameter(cf,'branchMin','float');
fillsmaller=getparameter(cf,'branchFill','float');
colprefix=getparameter(cf,'prefix','string');
connectivity=getparameter(cf,'connectivity','float');

if all(connectivity~=[4 8])
    writeerror(cf,'Connectivity must be 4 or 8.')
    return
end

imglist=getimagelist(imdir);
if numel(imorigdir)<2
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end

if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Source directories have different number of images')
    return
end
if numel(imglist)==0
    writeerror(cf,'No images in source directory')
    return
end
mkdir(outdir);
writeout.columnheads={'RowId','File','Object','Area','Filled area','Perimeter','Roundness','Solidity','Eccentricity', ... 
    'Ly','Lx','Alpha','Sk width','Sk width std', 'Skeleton length','Branch points','End points','Maximum length','Length estimate' ,...
    ['Distance(' num2str(nth) ')'],'X','Y'};
writeout.columnheads=cellfun(@(x) [colprefix x], writeout.columnheads,'UniformOutput',false);
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    if strcmpi(imglist{file}(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    [dataout, skelimg]=imagesinglemorph(id,nth,bramin,[imdirlist filesep imglist{file}],connectivity,fillsmaller);
    imwrite(skelimg,[outdir filesep imglist{file} pngstr]);
    dataout=num2csvcell(dataout);
    objs=size(dataout,1);
    dataoutname=repmat(origimglist(file),[objs 1]);
    dataoutname=[ strcat(dataoutname,'_',dataout(:,1)) dataoutname];  % Add the name_obj  id string
    if numel(dataout)>0
        writeout.data=[dataoutname dataout];
    else
        writeout.data={};
    end
    appendcsv(outfile,writeout);    
end

close all
toc
end

#!/usr/bin/python
import sys
from anduril.args import *
import anduril
import os
import shutil
import csv

def getfilelist(path):
    ''' Returns a list of files '''
    list=os.listdir(path)
    files=[]
    for fName in list:
        if fName[0] not in ['.','_']:
            files.append(fName)
    return files

def dotjoin(strList):
    ''' Joins a list of strings with dots, if list item exists. '''
    strout=''
    for i in range(len(strList)-1):
        if len(strList[i])>0:
            strout+=strList[i]+'.'
    strout+=strList[i+1]
    return strout

exts=[i.lower().strip() for i in extensions.split(',')]

os.mkdir(output_folder)
if output_folder is not None:
    inList=getfilelist(input_folder)
    for inName in inList:
        inSplit=inName.split('.')
        for c in range(1,len(inSplit)-1):
            if inSplit[c].lower() in exts:
                inSplit[c]=''
        outName=dotjoin(inSplit)
        print(inName+' -> '+outName)
        shutil.copy2(os.path.join(input_folder,inName), os.path.join(output_folder,outName))

if input_array is not None:
    for inName in input_array.items():
        inSplit=inName[0].split('.')
        for c in range(1,len(inSplit)-1):
            if inSplit[c].lower() in exts:
                inSplit[c]=''
        outName=dotjoin(inSplit)
        print(inName[0]+' -> '+outName)
        output_array.write(outName,inName[1])







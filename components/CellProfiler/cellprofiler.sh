
source "$ANDURIL_HOME/bash/functions.sh"

pipesrc=$( getinput pipeline )
location=$( getparameter path )
piperun=$( getoutput pipeline )
tempdir=$( gettempdir )

for ((i=1; i<10; i++))
do inname="inf"${i}
   outname="outf"${i}
   parname="par"${i}
   declare $inname=$( getinput folder${i} )
   declare $outname=$( getoutput folder${i} )
   declare $parname=$( getparameter param${i} )
   mkdir "${!outname}"
done

CELLPROFILER=${location}/CellProfiler.py
if [ ! -f "$CELLPROFILER" ]
then
    msg="CellProfiler not found in: $CELLPROFILER"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
fi
cp "$pipesrc" "$tempdir"/pipeline.cp

sed -e 's,@PARAMETER.param1@,'"$par1"',g' \
    -e 's,@PARAMETER.param2@,'"$par2"',g' \
    -e 's,@PARAMETER.param3@,'"$par3"',g' \
    -e 's,@PARAMETER.param4@,'"$par4"',g' \
    -e 's,@PARAMETER.param5@,'"$par5"',g' \
    -e 's,@PARAMETER.param6@,'"$par6"',g' \
    -e 's,@PARAMETER.param7@,'"$par7"',g' \
    -e 's,@PARAMETER.param8@,'"$par8"',g' \
    -e 's,@PARAMETER.param9@,'"$par9"',g' \
    -e 's,@INPUT.folder1@,'"$inf1"',g' \
    -e 's,@INPUT.folder2@,'"$inf2"',g' \
    -e 's,@INPUT.folder3@,'"$inf3"',g' \
    -e 's,@INPUT.folder4@,'"$inf4"',g' \
    -e 's,@INPUT.folder5@,'"$inf5"',g' \
    -e 's,@INPUT.folder6@,'"$inf6"',g' \
    -e 's,@INPUT.folder7@,'"$inf7"',g' \
    -e 's,@INPUT.folder8@,'"$inf8"',g' \
    -e 's,@INPUT.folder9@,'"$inf9"',g' \
    -e 's,@OUTPUT.folder1@,'"$outf1"',g' \
    -e 's,@OUTPUT.folder2@,'"$outf2"',g' \
    -e 's,@OUTPUT.folder3@,'"$outf3"',g' \
    -e 's,@OUTPUT.folder4@,'"$outf4"',g' \
    -e 's,@OUTPUT.folder5@,'"$outf5"',g' \
    -e 's,@OUTPUT.folder6@,'"$outf6"',g' \
    -e 's,@OUTPUT.folder7@,'"$outf7"',g' \
    -e 's,@OUTPUT.folder8@,'"$outf8"',g' \
    -e 's,@OUTPUT.folder9@,'"$outf9"',g' \
    "$tempdir"/pipeline.cp > "$piperun"
echo python $CELLPROFILER -b -c -r -i "$inf1" -o "$outf1" -p "$piperun"
python $CELLPROFILER -b -c -r -i "$inf1" -o "$outf1" -p "$piperun"
errcode=$?
#some heuristic error handling:  find which output folders are being used and check they contain files
for ((i=1; i<10; i++))
do outname="outf"${i}
   if ( grep "@OUTPUT.folder${i}@" "$tempdir"/pipeline.cp > /dev/null )
   then fc=$( ls "${!outname}" | wc --lines )
        echo "folder $i file count: $fc" >> $logfile
        echo $slsep >> $logfile
        if [ $fc -eq 0 ]
        then echo "NO files in output port folder"$i"!" >> $errorfile
             echo $slsep >> $errorfile
        fi
   fi
done
exit $errcode

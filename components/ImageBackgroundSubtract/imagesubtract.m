function imagesubtract(cf)

imdir=getinput(cf,'dir');
inputdir=getinput(cf,'input');
outdir=getoutput(cf,'dir');

par=getparameter(cf,'par','float');
method=lower(getparameter(cf,'method','string'));
maxit=getparameter(cf,'stretch','boolean');

mkdir(outdir);

imglist=getimagelist(imdir);
inputlist=getimagelist(inputdir);

for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    im_in=im2double(imread(id));
    im_final=im_in;
    channels=size(im_in, 3);
    for c=1:channels
        gray=im_in(:,:,c);
        switch method
            case 'constant'
                imout=gray-par;
            case 'mean'
                level=mean(gray(:))*par;
                imout=gray-level;
            case 'blur'
                par=round(par);
                imlow=bpass(padarray(gray,[par par],'replicate'),1,par);
                imout=imlow((par+1):(end-par),(par+1):(end-par));
            case 'gaussian'
                par=round(par);
                windowSize=4*par;
                imlow=imfilter(gray,fspecial('gaussian',[windowSize windowSize],par),'replicate');
                imout=gray-imlow;
            case 'open'
                par=round(par);
                imout=imopen(gray, strel('disk', par, 0));
            case 'close'
                par=round(par);
                imout=imclose(gray, strel('disk', par, 0));
            case 'input'
                inp=im2double(imread([inputdir filesep inputlist{file}]));
                imout=gray-inp(:,:,c);
            otherwise
                writeerror(cf,'Segmentation method was not recognized')
                return
        end
        imout(imout<0)=0; imout(imout>1)=1;
        if maxit
            imout=mat2gray(imout);
        end
        im_final(:,:,c)=imout;
    end
    imwrite(im_final,[outdir filesep imglist{file}]);

end

end

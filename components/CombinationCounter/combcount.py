#!/usr/bin/python
import sys
from anduril import *
from anduril.args import *

table=TableReader(input_data, column_types=str)

col1=table.get_column(param_col1)
col2=table.get_column(param_col2)

col1u=sorted(list(set(col1)))
col2u=sorted(list(set(col2)))
# calculate occurences of rows of all possible combinations of col1 vs col2
counts=[]
for c1 in range(len(col1u)):
    for c2 in range(len(col2u)):
        counts.append([
            col1u[c1],
            col2u[c2],
            sum([1 for v in zip(col1,col2) if v[0]==col1u[c1] and v[1]==col2u[c2]]),
            sum([1 for v in col1 if v==col1u[c1]])
        ])
ratios=[]
for c in counts:
    ratios.append(float(c[2])/float(c[3]))
# Write counts
fields=[param_col1, param_col2, "Count","Total","Ratio"]
table=TableWriter(output_data,fieldnames=fields)
for r in zip(counts,ratios):
    table.writerow(r[0]+[r[1]])

# Write ratios
table=TableWriter(output_ratios,fieldnames=[param_col1]+col2u)
i=0
for c1 in range(len(col1u)):
    table.writerow([
        col1u[c1]]+
        ratios[i:i+len(col2u)]
    )
    i+=len(col2u)



source "$ANDURIL_HOME/bash/functions.sh"
export_command

parameter_format=$( echo $parameter_format | tr A-Z a-z )
filename=$( echo ${parameter_namestring} | sed 's,@instance@,'${metadata_instanceName}',g' )

[ "$parameter_height" -gt 0 -a "$parameter_width" -gt 0 ] && resizeforce="!" || resizeforce=""
[ "$parameter_height" = "0" ] && parameter_height=""
[ "$parameter_width" = "0" ]  && parameter_width=""

if [ "$parameter_height" = "" ] && [ "$parameter_width" = "" ] 
then resizestring=""
else declare -a resizestring
     resizestring="-geometry ${parameter_width}x${parameter_height}${resizeforce}"
fi

if [ "$parameter_format" = "tif" ] || [ "$parameter_format" = "gif" ]
then CONVERT=$( which convert 2> /dev/null )
    if ( "$CONVERT" -version | grep -i imagemagick )
    then echo "ImageMagick found."
    else
    msg="ImageMagick convert not found. ( $CONVERT )"
    echo $msg >&2
    echo $msg >> "$errorfile"
    echo $slsep >> "$errorfile"
    exit 1
    fi
    medformat=miff
fi

if [ "$parameter_format" = "ffmp4" ] || [ "$parameter_format" = "x264" ] || [ "$parameter_format" = "mp4" ]
then AVCONV=$( which avconv 2> /dev/null )
    if ( "$AVCONV" -version 2>&1 | grep -qi avconv ) 2>&1 > /dev/null
    then echo "avconv found."
    else 
        AVCONV=$( which ffmpeg 2> /dev/null )
        if ( "$AVCONV" -version 2>&1 | grep -qi FFmpeg ) 2>&1 > /dev/null
        then echo "FFmpeg found."
        else
            msg=$AVCONV" not found. Maybe it is not installed on this system?"
            echo $msg >&2
            echo $msg >> "$errorfile"
            echo $slsep >> "$errorfile"
            exit 1
        fi
    fi
    medformat="png"
fi

if [ -z "$medformat" ]
then
    echo "Format \"$parameter_format\" not recognized" >> "$errorfile"
    exit 1
fi

mkdir "$output_video"
tempdir=$( gettempdir )

oldifs=$IFS
IFS=$'\n'
infiles=( $( ls -v1 "$input_dir" ) )
IFS=$oldifs
incount=${#infiles[@]}
for ((i=0;i<${incount};i++))
do
    convert ${resizestring} "${input_dir}/${infiles[$i]}" "${tempdir}/file"$((1+${i}))".$medformat" 2>&1 >> "$logfile" 
done
#ls -v $tempdir
# stack image formats:

if [ "$parameter_format" = "tif" ]
then ext=.tif
    "$CONVERT" -verbose "${tempdir}/file%d.${medformat}[1-${incount}]" -adjoin -compress None "${output_video}/${filename}${ext}" 2>&1 >> "$logfile" 
    echo $slsep >> "$logfile"
    if [ ! -s "${output_video}/${filename}${ext}" ]
    then echo error converting files  >> "$errorfile"
        exit 1
    fi
    exit 0
fi

if [ "$parameter_format" = "gif" ]
then ext=.gif
    delay=$( echo "scale=2; 100*1/$parameter_framerate" | bc -q 2>/dev/null )
    "$CONVERT" -verbose -delay $delay "${tempdir}/file%d.${medformat}[1-${incount}]" "${output_video}/${filename}${ext}" 2>&1 >> "$logfile"
    echo $slsep >> "$logfile"
    if [ ! -s "${output_video}/${filename}${ext}" ]
    then echo error converting files  >> "$errorfile"
        exit 1
    fi
    exit 0
fi

# True video formats:

if [ "$parameter_format" = "ffmp4" ]
then ext=.avi
    "$AVCONV" -r ${parameter_framerate} -i "${tempdir}/file%d.${medformat}" -b ${parameter_bitrate}k -r ${parameter_framerate} "${output_video}/${filename}${ext}" >> "$logfile" 2>> "$logfile"
    echo $slsep >> "$logfile"
fi

if [ "$parameter_format" = "x264" ]
then ext=.mp4
    "$AVCONV" -r ${parameter_framerate} -i "${tempdir}/file%d.${medformat}" -vcodec libx264 -vpre slow -threads 0 -crf ${parameter_crf} -r 24 "${output_video}/${filename}${ext}" >> "$logfile" 2>> "$logfile"
    echo $slsep >> "$logfile"
fi

if [ "$parameter_format" = "mp4" ]
then ext=.mp4
    "$AVCONV" -r ${parameter_framerate} -i "${tempdir}/file%d.${medformat}" -vcodec mpeg4 -b ${parameter_bitrate}k -r 24 "${output_video}/${filename}${ext}" >> "$logfile" 2>> "$logfile"
    echo $slsep >> "$logfile"
fi

if [ ! -s "${output_video}/${filename}${ext}" ]
then echo error converting files  >> "$errorfile"
    exit 1
fi
exit 0




function histogram2d(cf)
%
% portions of this code were adapted from:
% http://www.mathworks.com/support/solutions/data/1-2WPAYR.html?solution=1-2WPAYR
% cf = Anduril command file
tic
componentname='2D Histogram';
in=getinput(cf,'data');
outdir=getoutput(cf,'report');
colxname=getparameter(cf,'xCol','string');
colyname=getparameter(cf,'yCol','string');
colored=getparameter(cf,'color','boolean');
stitle=getparameter(cf,'title','string');
caption=getparameter(cf,'caption','string');
binx=getparameter(cf,'xBin','float');
biny=getparameter(cf,'yBin','float');
conditionals=getparameter(cf,'conditionals','boolean');
instance=getmetadata(cf,'instanceName');
fformat=getparameter(cf,'format','string');

data=readcsvcell(in,0,char(9));

if strcmp(colxname,'')
    colx=1;
    colxname=data.columnheads{1};
end
if strcmp(colyname,'')
    coly=2;
    colyname=data.columnheads{2};
end

dx=getcellcol(data,colxname,'float');
dy=getcellcol(data,colyname,'float');
clear data;
%%
%xmin=nanmean(d1)-3*nanstd(d1); xmax=xmin+6*nanstd(d1);
%ymin=nanmean(d2)-3*nanstd(d2); ymax=ymin+6*nanstd(d2);
xmin=min(dx); xmax=max(dx);
ymin=min(dy); ymax=max(dy);

xedge=linspace(xmin,xmax,binx);
yedge=linspace(ymin,ymax,biny);

h=figure(1); set(h,'Visible','off'); %set(h,'PaperOrientation','landscape'); 
title(stitle)
set(h,'PaperUnits','points'); 
set(h,'PaperPositionMode','auto')
set(h,'PaperOrientation','landscape');

hold off
C=hist3([dy,dx],'Edges',{yedge, xedge});
img=mat2gray(imresize(C,4,'nearest'));
if colored
    img=label2rgb(im2uint8(img),'jet','k');
end
imshow(img);
axis normal
axis xy
axis on
hold on;
ax1=gca;
set(ax1,'FontSize',22)
set(ax1,'Xtick',linspace(0.5,(binx)*4+0.5,5));
set(ax1,'XtickLabel', num2str(linspace(xmin,xmax,5)',2))
set(ax1,'Ytick',linspace(.5,(biny)*4+0.5,5));
set(ax1,'YtickLabel', num2str(linspace(ymin,ymax,5)',2))
set(ax1,'LineWidth',1.5);
xlabel(colxname)
ylabel(colyname)

%% conditional histograms
if conditionals
xhist=sum(C,1); xhistmax=max(xhist);
xhist=(4*.5*biny*xhist/xhistmax);  % normalize to reach halfway the figure
yhist=sum(C,2); yhistmax=max(yhist);
yhist=(4*.5*binx*yhist/yhistmax);
stairs(-3.5+4*(1:(binx)),xhist,'Color',[1 1 0],'LineWidth',2);
stairs([yhist(1); yhist],.5+4*(0:(biny)),'Color',[0 1 0],'LineWidth',2);
ax2=axes('Position',get(ax1,'Position'),'XAxisLocation','top','Color','none','YAxisLocation','right','YColor',[.5 .5 .5],'TickDir','out','XColor',[.5 .5 .5]);
xround=10^(numel(num2str(xhistmax,'%i'))-2);  
yticklabels=linspace(0,round(xhistmax/xround)*xround,6);
yticks=linspace(0,round(xhistmax/xround)*xround,6)./linspace(0,xhistmax,6).*linspace(0,0.5,6); yticks(1)=0;
yround=10^(numel(num2str(yhistmax,'%i'))-2);  
xticklabels=linspace(0,round(yhistmax/yround)*yround,6);
xticks=linspace(0,round(yhistmax/yround)*yround,6)./linspace(0,yhistmax,6).*linspace(0,0.5,6); xticks(1)=0;
set(ax2,'Xtick',xticks);
set(ax2,'XtickLabel', num2str(xticklabels' ,'%i'))
set(ax2,'Ytick',yticks);
set(ax2,'YtickLabel', num2str(yticklabels' ,'%i'))
end

% set(gca,'Units','points')
% gpos=get(gca,'Position')
% set(h,'Papersize', [gpos(4)+30 gpos(3)+30])

% set(gca,'PlotBoxAspectRatioMode','manual')
% set(gca,'DataAspectRatioMode','manual')
% axis fill
set(h,'Units','normalized');
set(h,'Position',[0.1 0.1 .8 .8]);
if strcmp(conditionals,'true')
set(ax1,'Position',[0.2 0.2 .6 .6]);
set(ax2,'Position',[0.2 0.2 .6 .6]);
end
% get(h,'Papersize')
% get(h,'Position')
% get(gca,'Position')

%%

hold off

% img=mat2gray(img);
% if strcmp(colored,'true')
%     img=label2rgb(im2uint8(img),'jet','k');
% end
% imwrite(img, [outfile filesep 'image.png']);
%print(h, '-dpdf','-r90',outfile);
writefigurelatex(outdir,h,[instance '-histogram2D.' lower(fformat)],fformat,caption,instance,componentname)
toc
end

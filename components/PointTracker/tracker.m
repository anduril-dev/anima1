function tracker(cf)

tic

csvfile=getinput(cf,'csv');
outfile=getoutput(cf,'csv');

dataname=getparameter(cf,'cCol','string');
objectname=getparameter(cf,'oCol','string');
timename=getparameter(cf,'tCol','string');
idname=getparameter(cf,'idCol','string');

param.mem=getparameter(cf,'memory','float');
param.quiet=1;
param.dim=0;
param.good=getparameter(cf,'goodLength','float');
maxdist=getparameter(cf,'maxDist','float');
% load data
csv=readcsvcell(csvfile,0,char(9));
try
    object=getcellcol(csv,objectname,'float');
catch ME
    writeerror(cf,['Problem with finding object column:' objectname]);
end
try
    time=getcellcol(csv,timename,'float');
catch ME
    writeerror(cf,['Problem with finding time column:' timename]);
end
try
    id=getcellcol(csv,idname,'string');
catch ME
    writeerror(cf,['Problem with finding row identificator column:' idname]);
end
remain=dataname;
data=zeros(numel(object),0);
colnames=cell(0);
while true
    [col, remain] = strtok(remain, ',');
    col=strtrim(col);
    if isempty(col),  break;  end
    try
        data=[data getcellcol(csv,col,'float')];
        param.dim=param.dim+1;
        colnames=[colnames {col}];
    catch ME
        writeerror(cf,['Problem with finding column: "' col '"']);
    end
end
clear csv;
[sorttime,sortidx]=sort(time);
object=object(sortidx);
data=data(sortidx,:);
id=id(sortidx,:);

tracked=track([data sorttime],maxdist,param);

% sorting of data to match the original IDs. it's a bit stupid but the algorithm doesn't return any ID info.
trow=size(tracked,1);
orow=size(data,1);
output_id=cell(trow,1);
output_object=zeros(trow,1);

for r=1:trow
    dists=sum(abs([data sorttime]-repmat(tracked(r,1:(end-1)),[orow 1])),2);
    [mindist, orig_idx]=min(dists);
    output_id(r)=id(orig_idx);
    output_object(r)=object(orig_idx);
end
tracked=[output_object tracked];

out.columnheads=[{idname,objectname} colnames {timename, 'TrackId'}];
out.data=[output_id num2csvcell(tracked)];
writefilecsv(outfile,out);

toc

end
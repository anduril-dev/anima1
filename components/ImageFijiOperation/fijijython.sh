#!/bin/bash
set +e
source "$ANDURIL_HOME/bash/functions.sh"
export PYTHONPATH=$ANDURIL_HOME/python:.:$PYTHONPATH
export JYTHONPATH=$ANDURIL_HOME/python:.:$JYTHONPATH

FIJIPATH=$( readlink -f ../../lib/Fiji.app )

export PATH=$PATH:$FIJIPATH

which fiji && FIJI=$( which fiji )
which ImageJ-linux64 && FIJI=$( which ImageJ-linux64 )

NOVIRTUAL=$( getparameter display )
if [ "$NOVIRTUAL" = "false" ]
then
    which Xvfb > /dev/null || {
        NOVIRTUAL=true
    }
fi

if [ "$NOVIRTUAL" = "false" ]
then 
    export DISPLAY=:$$
    rm -f /tmp/.X$$-lock
    ( Xvfb :$$ & ) 2> /dev/null
fi

"$FIJI" --mem=$( getparameter javaHeap )m --jython fijijython.py --allow-multiple --no-splash "$1" 
#> "$logfile"
exit_status=$?

if [ "$exit_status" -gt "0" ]
then echo "Component failed. Exit status: $exit_status" >> "$errorfile"
fi

if [ "$NOVIRTUAL" = "false" ]
then
    cat /tmp/.X$$-lock | xargs kill 
    rm -f /tmp/.X$$-lock
fi

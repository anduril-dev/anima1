function imageoperation(cf)
%
% cf = Anduril command file
tic
vlpath=['..' filesep '..' filesep 'lib' filesep 'vlfeat' filesep 'toolbox' ];
if exist(vlpath,'dir')
    addpath(vlpath)
end
if ~exist('vl_setup')
    writeerror(cf,['VLFeat library not found in path. Get the library from www.vlfeat.org and install to [bundle_root]' filesep 'lib' filesep 'vlfeat'])
    return
end
vl_setup
imdir=getinput(cf,'dir');
imorigdir=getinput(cf,'origdir');

outimage=getoutput(cf,'image');
outtable=getoutput(cf,'data');
method=lower(getparameter(cf,'method','string'));
sift_octaves=getparameter(cf,'sift_octaves','float');
sift_levels=getparameter(cf,'sift_levels','float');
sift_firstOctave=getparameter(cf,'sift_firstOctave','float');
sift_peakThresh=getparameter(cf,'sift_peakThresh','float');
sift_edgeThresh=getparameter(cf,'sift_edgeThresh','float');
sift_magnif=getparameter(cf,'sift_magnif','float');
sift_size=getparameter(cf,'sift_size','float');
dsift_step=getparameter(cf,'dsift_step','float');
mser_delta=getparameter(cf,'mser_delta','float');
mser_maxArea=getparameter(cf,'mser_maxArea','float');
mser_minArea=getparameter(cf,'mser_minArea','float');
mser_maxVariation=getparameter(cf,'mser_maxVariation','float');
mser_minDiversity=getparameter(cf,'mser_minDiversity','float');
mser_brightOnDark=getparameter(cf,'mser_brightOnDark','boolean');
mser_darkOnBright=getparameter(cf,'mser_darkOnBright','boolean');
mser_draw=lower(getparameter(cf,'mser_draw','string'));

mkdir(outimage);
mkdir(outtable);
if sum(strcmp(method,{'mser','sift','dsift'}))==0
    writeerror(cf,['Method "' method '" not supported'])
    return
end
if sum(strcmp({'regions','ellipses'},mser_draw))==0
    writeerror(cf,['MSER output "' mser_draw '" not supported'])
    return
end

imglist=getimagelist(imdir);
if (numel(imglist)==0)
    writeerror(cf,'No suitable images found')
end
if ~inputdefined(cf,'origdir')
    imorigdir=imdir;
    origimglist=imglist;
else
    origimglist=getexoticimagelist(imorigdir);
end
if numel(imglist)~=numel(origimglist)
    writeerror(cf,'Intensity image and name list directories have different number of images')
    return
end

lastrow=1;
for file=1:numel(imglist)
    id=[imdir filesep imglist{file}];
    disp(['Reading image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
    im=im2double(imread(id));
    if ndims(im)>2
        writeerror(cf,['Source image not gray scale! ' id]);
        im=rgb2gray(im);
    end
    [h w]=size(im);
    if strcmp('sift',method)
        if (sift_octaves==0)
            [objs_keys,objs_descr]=vl_sift(im2single(im),...
                'Levels',sift_levels,'FirstOctave',sift_firstOctave,...
                'PeakThresh',sift_peakThresh,'EdgeThresh',sift_edgeThresh,'Magnif',sift_magnif,'WindowSize',sift_size); %,'Frames',F,'Orientations');
        else
            [objs_keys,objs_descr]=vl_sift(im2single(im),...
                'Octaves',sift_octaves,'Levels',sift_levels,'FirstOctave',sift_firstOctave,...
                'PeakThresh',sift_peakThresh,'EdgeThresh',sift_edgeThresh,'Magnif',sift_magnif,'WindowSize',sift_size); %,'Frames',F,'Orientations'); 
        end
        objsindex=repmat(origimglist(file),[size(objs_descr,2) 1]);
        objsnums=(1:size(objs_descr,2))';
        if size(objs_descr,2)>0
            featureheads=strcat('Feature_', cellfun(@(x) sprintf('%d',x), num2cell(1:128),'UniformOutput',false));
            writeout.columnheads=[{'RowId','File','Key','X','Y','Scale','Orientation'} featureheads];
            writeout.data=cellfun(@(x) sprintf('%g',x), num2cell([objsnums (objs_keys)' double(objs_descr)']),'UniformOutput',false);
            writeout.data=[strcat(objsindex,'_',writeout.data(:,1)) objsindex writeout.data];
        end
        imout=zeros(size(im));
        %imout=im;
        %imout(im==1)=254/255;
        imout=drawboxes(imout,objs_keys);
    end
    if strcmp('dsift',method)
        
        im_scaled=vl_imsmooth(im,sqrt((sift_size/sift_magnif)^2 - .25));
        [objs_keys,objs_descr]=vl_dsift(im2single(im_scaled),...
                'Step',dsift_step,'Size',sift_size);
        objs_keys(3,:) = sift_size/sift_magnif ;
        objs_keys(4,:) = 0 ;
        objsindex=repmat(origimglist(file),[size(objs_descr,2) 1]);
        objsnums=(1:size(objs_descr,2))';
        imout=im_scaled;
        imout=drawboxes(imout,objs_keys);
        
        featureheads=strcat('Feature_', cellfun(@(x) sprintf('%d',x), num2cell(1:128),'UniformOutput',false));
        writeout.columnheads=[{'RowId','File','Key','X','Y','Scale','Orientation'} featureheads];
        writeout.data=cellfun(@(x) sprintf('%g',x), num2cell([objsnums (objs_keys)' double(objs_descr)']),'UniformOutput',false);
        writeout.data=[strcat(objsindex,'_',writeout.data(:,1)) objsindex writeout.data];
        
    end
    if strcmp('mser',method)
        im=im2uint8(im);
        [regions,ellipses]=vl_mser(im,...
                'Delta',mser_delta,'MaxArea',mser_maxArea,'MinArea',mser_minArea,...
                'MaxVariation',mser_maxVariation,'MinDiversity',mser_minDiversity,...
                'DarkOnBright',double(mser_darkOnBright),'BrightOnDark',double(mser_brightOnDark));
        
        ellipses=vl_ertr(ellipses)';
        objsindex=repmat(origimglist(file),[size(regions,1) 1]);
        objsnums=(1:size(regions,1))';
        imout=im2uint8(zeros(size(im)));
        
        switch mser_draw
            case 'regions'
                for x=regions'
                    s=vl_erfill(im,x);
                    imout(s)=1+imout(s);
                end
            case 'ellipses'
                [allx,ally]=pixelframe(ellipses',max(w,h));
                allx=max(1,allx); allx=min(w,allx);
                ally=max(1,ally); ally=min(h,ally);
                allind=sub2ind([h w],ally, allx);
                imout(allind)=255;
        end
        imout=mat2gray(im2double(imout));
        %imout=im2double(label2rgb(imout,'jet','k'));
        
        writeout.columnheads=[{'RowId','File','Key','Region Seed','X','Y','Cov 11','Cov 12','Cov 22'}];
        writeout.data=cellfun(@(x) sprintf('%g',x), num2cell([objsnums regions ellipses]),'UniformOutput',false);
        writeout.data=[strcat(objsindex,'_',writeout.data(:,1)) objsindex writeout.data];
        %writeout.columnheads=strcat('Feature_', cellfun(@(x) sprintf('%d',x), num2cell(1:size(writeout.data,2)),'UniformOutput',false));
        
    end
    if exist('imout','var')
        imout=min(1,imout);
        imout=max(0,imout);
        if (strcmp(imglist{file}((end-2):end),'png'))
            imwrite(imout,fullfile(outimage,imglist{file}));
        else
            imwrite(imout,fullfile(outimage,[imglist{file} '.png']));
        end
    end
    if exist('writeout','var')
        writefilecsv(fullfile(outtable,[imglist{file} '.csv']),writeout)
        clear writeout;
    end
end
toc


end % main func

function im=drawboxes(im,keys)
    [h,w]=size(im);
    for f=1:size(keys,2)
        cx=(keys(1,f));
        cy=(keys(2,f));
        s=sqrt(2)*keys(3,f);
        acc=round(2*s);
        phi=sin(keys(4,f));
        im(round(cy),round(cx))=0.5;
        px(1)=cx+s*sin(phi+1*pi/4);
        py(1)=cy+s*cos(phi+1*pi/4);
        px(2)=cx+s*sin(phi+3*pi/4);
        py(2)=cy+s*cos(phi+3*pi/4);
        px(3)=cx+s*sin(phi-3*pi/4);
        py(3)=cy+s*cos(phi-3*pi/4);            
        px(4)=cx+s*sin(phi-1*pi/4);
        py(4)=cy+s*cos(phi-1*pi/4);            
        px(5)=px(1); py(5)=py(1);
        px(6)=cx+(s-2)*sin(phi+pi/4);
        py(6)=cy+(s-2)*cos(phi+pi/4);
        px(7)=cx+(s-2)*sin(phi-pi/4);
        py(7)=cy+(s-2)*cos(phi-pi/4);            
        lx=[linspace(px(1),px(2),acc),
            linspace(px(2),px(3),acc),
            linspace(px(3),px(4),acc),
            linspace(px(4),px(5),acc),
            linspace(px(6),px(7),acc),];
        ly=[linspace(py(1),py(2),acc),
            linspace(py(2),py(3),acc),
            linspace(py(3),py(4),acc),
            linspace(py(4),py(5),acc),
            linspace(py(6),py(7),acc)];
        lx=max(1,round(lx)); lx=min(w,lx);
        ly=max(1,round(ly)); ly=min(h,ly);
        pyx=sub2ind([h w],ly,lx);
        im(pyx)=1;
    end

end % drawboxes

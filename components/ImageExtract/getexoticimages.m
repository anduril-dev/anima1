function numImages=getexoticimages(id,s)

javaaddpath(regexprep(mfilename('fullpath'),'getexoticimages$','loci_tools.jar'));
r = loci.formats.ChannelSeparator();
r.setId(id);
numSeries = r.getSeriesCount();

r.setSeries(s - 1);
numImages = r.getImageCount();

r.close;

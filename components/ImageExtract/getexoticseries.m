function numSeries=getexoticseries(id)

javaaddpath(regexprep(mfilename('fullpath'),'getexoticseries$','loci_tools.jar'));
r = loci.formats.ChannelSeparator();
r.setId(id);
numSeries = r.getSeriesCount();

r.close;

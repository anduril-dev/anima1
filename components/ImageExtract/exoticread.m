function [arr,meta]=exoticread(id,ch,s,givenbitdepth)

javaaddpath(regexprep(mfilename('fullpath'),'exoticread$','loci_tools.jar'));
r = loci.formats.ChannelSeparator();
r.setId(id);
numSeries = r.getSeriesCount();

r.setSeries(s - 1);
w = r.getSizeX();
h = r.getSizeY();
numImages = r.getImageCount();
fprintf('Reading series #%d', s);
fprintf('/%d, channel #%d/%d',numSeries,ch,numImages);
disp('.');
img = r.openImage(ch-1);
% convert Java BufferedImage to MATLAB image
B = img.getData.getPixels(0, 0, w, h, []);
plane=reshape(B, [w h])';
% arr(:,:,i+1)=plane;
arr=plane;
%end

metadataList = r.getMetadata();
metadataList=char(metadataList);
meta={};
bitfound=false;
if numel(metadataList)>3
    rem=regexprep(metadataList,{'^{','}$'},'');
    rem=regexprep(rem,', ','|');
    index=1;
    while rem
        [tok, rem]=strtok(rem,'|');
        [meta{index,1},meta{index,2}]=strtok(strtrim(tok),'=');
        index=index+1;
    end
    meta(:,2)=regexprep(meta(:,2),'^=','');
    [bitdepth,bitfound]=getmeta(meta,'Acquisition Bit Depth 0');
    if ~bitfound
        [bitdepth,bitfound]=getmeta(meta,'Bits per pixel');
    end
end

if givenbitdepth==0
    if bitfound
        arr=im2double(arr./(2.^bitdepth -1));
    else
        error('anduril:error', 'Bitdepth could not be read from image, give as parameter.')
    end
else
    arr=arr./(2.^givenbitdepth -1);
end

r.close;
end

function [value,ok]=getmeta(meta,key)
    value=str2double(getvariable(meta,key));
    ok=false;
    if and(isnumeric(value),~isnan(value))
        ok=true;
    end
end

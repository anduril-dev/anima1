#!/bin/bash
source "$ANDURIL_HOME/bash/functions.sh"
export PYTHONPATH=$ANDURIL_HOME/python:.:$PYTHONPATH
export JYTHONPATH=$ANDURIL_HOME/python:.:$JYTHONPATH

FIJIPATH=$( readlink -f ../../lib/Fiji.app )

export PATH=$PATH:$FIJIPATH

which fiji && FIJI=$( which fiji )
which ImageJ-linux64 && FIJI=$( which ImageJ-linux64 )

"$FIJI" --mem=$( getparameter javaHeap )m --jython fijibioformats.py "$1"
#> "$logfile"
exit_status=$?

if [ "$exit_status" -gt "0" ]
then echo "Component failed. Exit status: $exit_status" >> "$errorfile"
fi

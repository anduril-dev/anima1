import component_skeleton.main
from component_skeleton.Array import ArrayReader
import os
import re
import string
from ij import IJ
from ij.io import FileSaver
from loci.plugins import BF
from loci.plugins.in import ImporterOptions
from fijisave import save_image
import csv

def execute(cf):    
    ch = cf.get_parameter('ch', 'int')
    extension = cf.get_parameter('extension', 'string')
    useseriesname = False
    dirin = cf.get_input('dir')
    dirout = cf.get_output('channel')
    os.mkdir(dirout)
    tblfile = cf.get_output('table')
    
    iops = ImporterOptions()
    iops.setOpenAllSeries(True)
    iops.setSplitFocalPlanes(True)
    iops.setSplitTimepoints(True)
    iops.setSplitChannels(True)
    iops.setColorMode(iops.COLOR_MODE_GRAYSCALE)    
    
    files = os.listdir(dirin)
    files.sort()
    filenumber=1
    seriesnumber=1
    filetable = [["Containing file", "Extracted file", "Series name", "Series number", "Z coordinate", "Time", "Channel"]]
    for filename in files:
        iops.id = os.path.join(dirin, filename)
        print 'Opening:', iops.id
        imps = BF.openImagePlus(iops)
        if len(imps[1].getTitle().split(" - ")) > 2:
            hasSeries = True
        else:
            hasSeries = False
        seriesnumber = 0
        seriesname = ""
        extractednumber=0
        for imp in imps:
            title = imp.getTitle()
            if hasSeries:
                rexp = r"^%s - (.+) - Z=(\d+) T=(\d+) C=(\d+)$" % (re.escape(filename))
            else:
                rexp = r"^(.+) - Z=(\d+) T=(\d+) C=(\d+)$"
            m = re.search(rexp, title)
            if m.group(1) != seriesname:
                seriesnumber += 1
                seriesname = m.group(1)
            Z = int(m.group(2))+1
            T = int(m.group(3))+1
            C = int(m.group(4))+1
            if C == ch or ch == -1:
                extractednumber+=1
                if useseriesname:
                    seriesfname = ''.join([c if c.isalpha() or c.isdigit() else "_" for c in seriesname])
                    outfile = filename+"_%s_Z%i_T%i_C%i" % (seriesfname,Z,T,C)
                else:
                    outfile = filename+"_S%i_Z%i_T%i_C%i" % (seriesnumber,Z,T,C)               
                print 'Writing:', outfile+extension, '%i/%i %i/%i' % (extractednumber, len(imps), filenumber,len(files))
                save_image(imp, dirout, outfile, extension)
                filetable.append([filename, outfile+extension, seriesname, seriesnumber, Z, T, C])
        if extractednumber == 0:
            print 'Did not find requested plane: ',filename,' Ch:',ch
        filenumber+=1
    writer = csv.writer(open(tblfile,'wb'),delimiter='\t',quotechar='"',quoting=csv.QUOTE_NONNUMERIC)
    writer.writerows(filetable)
    return 0
    
component_skeleton.main.main(execute)

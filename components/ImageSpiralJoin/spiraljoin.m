function spiraljoin(cf)
%
% cf = Anduril command file
tic
imdir=getinput(cf,'dir');
outdir=getoutput(cf,'dir');
mkdir(outdir);
sformat=getparameter(cf,'format','string');
if numel(sformat)==0
    writeerror(cf,'The numbering format must be given!');
    exit
end
width=getparameter(cf,'size','float');

imglist=getimagelist(imdir);
if numel(imglist)>1
    join=true;
end
if numel(imglist)==1
    join=false;
end
if numel(imglist)==0
    writeerror(cf,'No valid image files found');
    exit
end

if join
    sformat=strrep(sformat,'.*','%f');
    joiner(cf,imdir,imglist,sformat,outdir);
else
    sformat=strrep(sformat,'.*','%02d');
    splitter(cf,imdir,imglist,sformat,outdir,width);
end
toc
end

function joiner(cf,imdir,contents,sformat,outdir)
    location=-1;
    for imgn=1:numel(contents)
        location=max(location,sscanf(contents{imgn},sformat));
    end          
    numofimgs=location+1;
    if round(sqrt(numofimgs)) ~= sqrt(numofimgs)
        writelog(cf,'Image count does not produce a square');
        numofimgs=ceil(sqrt(numofimgs)).^2;
        %exit
    end
    id=[imdir filesep contents{1}];
    img=imread(id);
    w=size(img,2);
    h=size(img,1);
    
    result=(zeros(h*sqrt(numofimgs), w*sqrt(numofimgs)));
    spirlmatr=spiral(sqrt(numofimgs))-1;
    
    for imgn=1:numel(contents)
    %ex. MFGTMP_110707130001_B01f1%dd0.C01
      location=sscanf(contents{imgn},sformat);
      if numel(location)==0
            writeerror(cf,'Number was not extracted: ')
            writeerror(cf,['Format: ' sprintf('%s', sformat)])
            writeerror(cf,['File: ' contents{imgn}])
            exit
      end
      id=[imdir filesep contents{imgn}];
      img=im2double(imread(id));
      disp(['Reading image ' contents{imgn} ' (' num2str(location)  ')  ' num2str(imgn) '/' num2str(numel(contents))]);
      if size(img,1)~=h
            writeerror(cf,'Image size changed!')
            exit
      end
      if size(img,2)~=w
            writeerror(cf,'Image size changed!')
            exit
      end
      [y,x]=find(spirlmatr==(location));
      result(1+(y-1)*h:y*h, 1+(x-1)*w:x*w)=img;
    end
    imwrite(result, [outdir filesep sprintf(sformat,[])]);

end

function splitter(cf,imdir,contents,sformat,outdir,width)
    if width==0
        writeerror(cf,'You must provide output image size!');
        exit
    end
    id=[imdir filesep contents{1}];
    img=imread(id);
    h=size(img,1);
    w=size(img,2);
    if ~and(mod(w,width)==0,mod(h,width)==0)
        writeerror(cf,'Merged image size is not a multiple of given split size!');
        exit
    end
    numofimgs=w/width * h/width;
    disp(['Number of images: ' num2str(numofimgs)])
    if strcmpi(sformat(end-2:end),'png')
        pngstr='';
    else
        pngstr='.png';
    end
    spirlmatr=spiral(sqrt(numofimgs))-1;
    for imgn=1:numofimgs
    %ex. MFGTMP_110707130001_B01f1%dd0.C01
      location=imgn-1; 
      id=[outdir filesep sprintf(sformat,location) pngstr];
      [y,x]=find(spirlmatr==(location));
      imgout=img(1+(y-1)*width:y*width, 1+(x-1)*width:x*width,:);
      disp(['Writing image ' sprintf(sformat,location) ' ' mat2str(imgn) '/' mat2str(numofimgs)]);
      imwrite(imgout, id);
    end


end

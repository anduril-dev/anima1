
source "$ANDURIL_HOME/bash/functions.sh"

FARSIGHTPATH=$( readlink -f ../../lib/Farsight )

export PATH=$PATH:$FARSIGHTPATH/bin

# make the list separator a rowchange
IFS=$'\n' 

indir=$( getinput dir )

maskdir=$( getoutput mask )
perimdir=$( getoutput perimeter )

SNbin=$( getparameter binary )
magickbin="convert"

params="high_sensitivity	 :	$( [ $( getparameter high_sensitivity ) = true ] && echo 1 || echo 0 )
LoG_size	         :	30
min_scale	         :	$( getparameter min_scale )
max_scale	         :	$( getparameter max_scale )
xy_clustering_res	 :	$( getparameter xy_clustering_res )
z_clustering_res	 :	$( getparameter z_clustering_res )
finalize_segmentation	 :	$( [ $( getparameter finalize_segmentation ) = true ] && echo 1 || echo 0 )
sampling_ratio_XY_to_Z   :	$( getparameter sampling_ratio_XY_to_Z )
Use_Distance_Map	 :	$( [ $( getparameter Use_Distance_Map ) = true ] && echo 1 || echo 0 )
refinement_range	 :	$( getparameter refinement_range )"

tempdir=$( gettempdir )

CONVERT=$( which $magickbin )
if ( "$CONVERT" -version | grep -i imagemagick )
    then writelog "ImageMagick found."
else
    writeerror "ImageMagick $magickbin not found. ( $CONVERT )"
    exit 1
fi
SN=$( which $SNbin )
if ( "$SN" | grep -i usage1 )
    then writelog "segment_nuclei found."
else
    writeerror "FARSight segment_nuclei not found. ( $SNbin )"
    exit 1
fi


# list files
if [ -d "$indir" ]
then infiles=( $( ls -1 "$indir" ) ) || echo "error reading input dir" >> "$errorfile"
fi

# create directories
mkdir "${maskdir}" || writeerror "error creating directory ${maskdir}"
mkdir "${perimdir}" || writeerror "error creating directory ${perimdir}"

writelog "$params"
# loop over images
for (( f=0; f<${#infiles[@]}; f++ ))
    do writelog "$(( $f+1 )) / ${#infiles[@]} : ${infiles[$f]}"
    # Convert to 8bit grayscale
    "$CONVERT" "${indir}/${infiles[$f]}" -type GrayScale -colorspace Gray -verbose PNG8:"$tempdir"/gray.png
    echo "$params" > "${tempdir}/segment_params.txt"
    "$SN" "${tempdir}/gray.png" "${tempdir}/labels.tif" "${tempdir}/segment_params.txt" >> "${tempdir}/log"
    # Segmentation fails
    if [ $? -eq 139 ]
    then "$CONVERT" "${tempdir}/gray.png" -fx 0 +compress "${tempdir}/labels.tif"
         writelog "Segmenting \"${infiles[$f]}\" produced zero result, creating empty mask."
    fi
    if [ ! -f "${tempdir}/labels.tif" ]
    then writeerror "Segmenting \"${infiles[$f]}\" failed!"
         exit 1
    fi
    "$CONVERT" "${tempdir}/labels.tif" -morphology EdgeIn Disk:1 -threshold 0 "${perimdir}/${infiles[$f]}" 2>> "$logfile"
    "$CONVERT" -compose Multiply \( "${tempdir}/labels.tif" -threshold 0 \) \
            \( "${perimdir}/${infiles[$f]}" -negate \) -composite "${maskdir}/${infiles[$f]}"

    rm -f "${tempdir}/"*dat
    rm -f "${tempdir}/"*tif
    rm -f "${tempdir}/"*png
    if [ ! -f "${maskdir}/${infiles[$f]}" ]
    then writeerror "Can not find result of \"${infiles[$f]}\" !"
         exit 1
    fi
done


addpath('../../lib/matlab/')
try
    cf=readcommandfile(commandfile);  

    imdir=getinput(cf,'dir');
    outmask=getoutput(cf,'mask');
    outperim=getoutput(cf,'perimeter');
    outmasklist=getoutput(cf,'masklist');
    csvfile=getinput(cf,'objects');
    imorigdir=getinput(cf,'origdir');
    arrow=getparameter(cf,'arrow','boolean');
    masklist=getparameter(cf,'masklist','boolean');
    
    tic
    imglist=getimagelist(imdir);
    if numel(imorigdir)<2
        imorigdir=imdir;
        origimglist=imglist;
    else
        origimglist=getexoticimagelist(imorigdir);
    end
    
    if numel(imglist)~=numel(origimglist)
        writeerror(cf,'Image name list and gray scale list numbers do not match');
        disp('Image name list and gray scale list numbers do not match');
        return;
    end
    
    blobdata=readcsvcell(csvfile);
    %data.object=getcellcol(blobdata,'Object','float');
    data.file=getcellcol(blobdata,'File','string');
    data.alpha=getcellcol(blobdata,'Alpha','float');
    data.lx=getcellcol(blobdata,'Lx','float');
    data.ly=getcellcol(blobdata,'Ly','float');
    data.cy=getcellcol(blobdata,'Y','float');
    data.cx=getcellcol(blobdata,'X','float');
    clear blobdata;
    %% image looping
    mkdir(outmask)
    mkdir(outperim)
    mkdir(outmasklist)
    
    for file=1:numel(imglist)
        id=[imdir filesep imglist{file}];
        disp(['Read image: ' imglist{file} ' (' num2str(file) '/' num2str(numel(imglist)) ')']);
        if strcmpi(imglist{file}(end-2:end),'png')
            pngstr='';
        else
            pngstr='.png';
        end
        im=(imread(id));
        [height,width,foo] = size(im);
        maskim=false([height width]); % just to make sure it's grayscale...
        perim=false([height width]); % just to make sure it's grayscale...
        rows=strcmp(origimglist{file},data.file);
            
        alpha=data.alpha(rows);
        lx=data.lx(rows);
        ly=data.ly(rows);
        cy=data.cy(rows);
        cx=data.cx(rows);
            
        ellipseprec=max(32,round(8*max([lx;ly])));
        [imX,imY]=meshgrid(1:width,1:height);
        [Xbar,Ybar]=getedges(cx,cy,alpha,lx,ly,ellipseprec,width,height,arrow);
        Ybar=floor(Ybar);
        Xbar=floor(Xbar);
        edgeidx=sub2ind([height width],Ybar,Xbar);
        perim(edgeidx)=true;
        if masklist
            mkdir([outmasklist filesep imglist{file} pngstr]);
            for c=1:size(Xbar,1)
                maskobj=false([height width]);
                xl=min(Xbar(c,:)); xh=max(Xbar(c,:));
                yl=min(Ybar(c,:)); yh=max(Ybar(c,:));
                maskin=inpolygon(imX(yl:yh,xl:xh),imY(yl:yh,xl:xh),Xbar(c,:),Ybar(c,:));
                maskobj(yl:yh,xl:xh)=maskin;
                maskim(yl:yh,xl:xh)=or(maskin,maskim(yl:yh,xl:xh));
                imwrite(maskobj,[outmasklist filesep imglist{file} pngstr filesep num2str(c) '.png' ]);
            end
        else % not writing masklists
            for c=1:size(Xbar,1)
                maskobj=false([height width]);
                this_edges=sub2ind([height width], Ybar(c,:), Xbar(c,:));
                maskobj(this_edges)=true;
                maskim=or(maskim, imfill(maskobj, 'holes'));
            end
        end
        imwrite(maskim,[outmask filesep imglist{file} pngstr]);
        imwrite(perim,[outperim filesep imglist{file} pngstr]);
    end
    toc
%%% End of component code   

catch me  % let the rest be as it is, it is for error handling
    try
        writeerrorstack(cf, me);
    catch again
        exit
    end
end
exit;  % the script must exit matlab at the end!


source "$ANDURIL_HOME/bash/functions.sh"
export_command

mkdir "${output_masklist}"
mkdir "${output_mask}"
mkdir "${output_perimeter}"

IFS=$'\n'

for file in $( ls "${input_mask}" )
do  tgt=$( echo $file | sed 's,^\([0-9]\+\)_\(.*\),\2/\1,' )
    tgtdir=$( dirname "${output_masklist}"/"$tgt" )
    [[ -d "${tgtdir}" ]] || basename "${tgtdir}"
    mkdir -p "${tgtdir}"
    cp "${input_mask}"/"${file}" "${output_masklist}"/"$tgt".png
done

## normal operation stop here.
## If "join"  convert to mask and perimeter: 

if [ "$parameter_join" == "true" ]
then magickbin="convert"
    CONVERT=$( which $magickbin )
    TMP=$( gettempdir )
    if ( "$CONVERT" -version | grep -i imagemagick )
        then writelog "ImageMagick found."
    else
        writeerror "ImageMagick $magickbin not found. ( $CONVERT )"
        exit 1
    fi
    for dir in $( ls "${output_masklist}" )
    do  m_tgt="${output_mask}"/"$dir".png
        p_tgt="${output_perimeter}"/"$dir".png
        convert "${output_masklist}"/"$dir"/* -evaluate-sequence Max "${m_tgt}"
        cp "${output_masklist}"/"$dir"/* "$TMP"/
        mogrify -morphology EdgeIn Disk:1 -threshold 0 "$TMP"/*
        convert "$TMP"/* -evaluate-sequence Max -verbose  "${p_tgt}"
        rm "$TMP"/*
    done 
fi


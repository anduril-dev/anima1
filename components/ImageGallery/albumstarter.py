#!/usr/bin/env python
#
# Copyright 2012 Ville Rantanen
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys,os
import anduril
from anduril.args import *
import shutil
import csv
import string
cf = anduril.CommandFile.from_file(sys.argv[1])

# (c) ville.rantanen@helsinki.fi

LIBPATH='../../lib/qalbum'
if os.path.exists(LIBPATH):
    os.chdir(LIBPATH)
    sys.path.append('.')

try:
    import Qalbum
except ImportError as err:
    print('Install Qalbum to '+os.path.abspath(LIBPATH))
    print('Or add it to python path.')
    print(err)
    sys.exit(1)

def copytree(src, dst, symlinks=False):
    """Recursively copy a directory tree using shutil.copy2().

    Modified from shutil.copytree to overcome copystat -problems
    on NTFS / CIFS / other non-octect permission media
     ( uses Anduril logger )

    """
    names = os.listdir(src)
    os.makedirs(dst)
    errors = []
    permission_errors = []
    for name in names:
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree(srcname, dstname, symlinks)
            else:
                # Will raise a SpecialFileError for unsupported file types
                shutil.copyfile(srcname, dstname)
                try:
                    shutil.copystat(srcname, dstname)
                except:
                    # stats may not copy.
                    permission_errors.append(dstname)
                    pass
        # catch the Error from the recursive copytree so that we can
        # continue with other files
        except Error, err:
            errors.extend(err.args[0])
        except EnvironmentError, why:
            errors.append((srcname, dstname, str(why)))
    try:
        shutil.copystat(src, dst)
    except:
        # If permissions cannot be set, skip this
        permission_errors.append(dst)
        pass

    if len(permission_errors)>0:
        cf.write_log('Could not change permissions: '+', '.join(permission_errors))

    if errors:
        raise Error, errors

def valid_filename(s):
    """ Return a valid filename from a string """
    valid_chars = "-_.()[] %s%s" % (string.ascii_letters, string.digits)
    return ''.join(c for c in s if c in valid_chars)


inputs=[]
inputs.append((cf.get_input('folderRoot'),'',cf.get_input('csvRoot')))
for i in range(8):
    inputs.append((cf.get_input('folder'+str(i+1)),
                   valid_filename(cf.get_parameter('title'+str(i+1))),
                   cf.get_input('csv'+str(i+1))))
if folderArray != None:
    for a in folderArray:
        inputs.append((folderArray[a],valid_filename(a),cf.get_input('csvRoot')))
options=Qalbum.setupoptions()
options.gallery=titleRoot
options.parent=parent
options.timesort=sortTime
options.reverse=sortReverse
options.width=width
options.link=mediumLink
options.gravity=gravity

if style:
    options.style=style
else:
    options.style='gallerystyle.css'
options=Qalbum.setupdefaultoptions(options)
outDir = gallery

# the folderRoot demands that the outDir is not created earlier.
if inputs[0][0]==None:
    os.mkdir(outDir)
else:
    copytree(inputs[0][0],os.path.join(outDir,inputs[0][1]),symlinks=True)
if cf.get_input('infoRoot'):
    shutil.copyfile(infoRoot,os.path.join(outDir,'info.txt'))

for d in inputs:
    if (d[0]==None):
        continue
    if not os.path.exists(d[0]):
        continue
    print('Copying gallery '+d[1])
    cf.write_log('Copying gallery '+d[1])
    if not os.path.exists(os.path.join(outDir,d[1])):
        copytree(d[0],os.path.join(outDir,d[1]),symlinks=True)
    # Find the annotations
    if (d[2] is not None): 
        reader = csv.DictReader(open(d[2],'rb'),
                                delimiter='\t',
                                doublequote=False,
                                escapechar='\\',
                                quoting=csv.QUOTE_ALL)
        for row in reader:
            break
        if (fileCol not in reader.fieldnames):
            cf.write_error("Column \""+fileCol+"\" not found.")
            exit()
        if (annotationCol not in reader.fieldnames):
            cf.write_error("Column \""+annotationCol+"\" not found.")
            exit()
        reader = csv.DictReader(open(d[2],'rb'),
                                delimiter='\t',
                                doublequote=False,
                                escapechar=None,
                                quotechar='"',
                                quoting=csv.QUOTE_NONE)
        annotations=[]
        header=['file','description']
        try:
            for row in reader:
                annotations.append( (Qalbum.stripquotes.sub('',row.get('"'+fileCol+'"')), Qalbum.stripquotes.sub('',row.get('"'+annotationCol+'"')) ) )
        except:
            cf.write_error("Could not read file "+d[2])
            cf.write_error("(Check quoting)")
            exit()
        writefid=open(os.path.join(outDir,d[1],'descriptions.csv'),'wb')
        writer = csv.writer(writefid,
                        delimiter='\t',
                        doublequote=False,escapechar='\\',
                        quotechar='"',
                        quoting=csv.QUOTE_NONE)
        writer.writerow(header)
        for r in annotations:
            writer.writerow([r[0],r[1]])
        writefid.close()
# Copy all resources to target folder
shutil.copyfile(options.style,os.path.join(outDir,'gallerystyle.css'))
shutil.copyfile('galleryscript.js',os.path.join(outDir,'galleryscript.js'))

inputs[0]=((folderRoot,titleRoot,csvRoot))
options.startpath=os.path.abspath(outDir)
Qalbum.traverse(options.startpath,[options.startpath],inputs,options)

if includeZip:
    print('Archiving as ZIP')
    import zipfile
    zip_name=os.path.join(outDir,titleRoot+".zip")
    zip_writer=zipfile.ZipFile(zip_name,'w',zipfile.ZIP_DEFLATED)
    for path,dirs,files in os.walk(outDir):
        relpath=os.path.relpath(path,outDir)
        files.sort()
        dirs.sort()
        zip_writer.write(path,relpath)
        for fn in files:
            if os.path.samefile( zip_name, os.path.join(path,fn)):
                continue
            zip_writer.write(os.path.join(path,fn),os.path.join(relpath,fn))
    zip_writer.close()
    # regenerate HTML with the archive itself, without printing
    sys.stdout = open(os.devnull, 'w')
    Qalbum.traverse(options.startpath,[options.startpath],inputs,options)
    sys.stdout = sys.__stdout__

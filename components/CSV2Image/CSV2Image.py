#!/usr/bin/python
import sys
import anduril
from anduril.args import *
#import component_skeleton.main
import os
import scipy.misc
import numpy as n
import csv
import anduril.imagetools as imgtool
#import component_skeleton.ImageTools as imgtool
import re

def hsv2rgb(arr):
    """HSV to RGB color space conversion.

    References
    [0] http://www.nullege.com/codes/show/src@scikits.image-0.2.2@scikits@image@color@colorconv.py/299/numpy.swapaxes
    [1] http://en.wikipedia.org/wiki/HSL_and_HSV

    """
    hi = n.floor(arr[:,:,0] * 6)
    f = arr[:,:,0] * 6 - hi
    p = arr[:,:,2] * (1 - arr[:,:,1])
    q = arr[:,:,2] * (1 - f * arr[:,:,1])
    t = arr[:,:,2] * (1 - (1 - f) * arr[:,:,1])
    v = arr[:,:,2]

    hi = n.dstack([hi, hi, hi]).astype(n.uint8) % 6
    out = n.choose(hi, [n.dstack((v, t, p)),
                         n.dstack((q, v, p)),
                         n.dstack((p, v, t)),
                         n.dstack((p, q, v)),
                         n.dstack((t, p, v)),
                         n.dstack((v, p, q))])
    return out

inFile = input_csv#cf.get_input('csv')
#vCol = cf.get_parameter('vCol')
#xCol = cf.get_parameter('xCol')
#yCol = cf.get_parameter('yCol')
#colored = cf.get_parameter('color','boolean')
#width = cf.get_parameter('width','float')
#height = cf.get_parameter('height','float')
imin = iMin#cf.get_parameter('iMin','float')
imax = iMax#cf.get_parameter('iMax','float')
outFile = re.sub('[^-a-zA-Z0-9_.() ]+', '_', vCol)

outDir = image#cf.get_output('image')
freqDir = frequency#cf.get_output('frequency')
os.mkdir(outDir)
os.mkdir(freqDir)
# read CSV file
reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
for row in reader:
    break
# ^ make sure fieldnames are initialized...

if (vCol not in reader.fieldnames):
        msg="Column \""+vCol+"\" not found."
        print(msg)
        cf.write_error(msg)
        exit()
if (xCol not in reader.fieldnames):
        msg="Column \""+xCol+"\" not found."
        print(msg)
        cf.write_error(msg)
        exit()            
if (yCol not in reader.fieldnames):
        msg="Column \""+yCol+"\" not found."
        print(msg)
        cf.write_error(msg)
        exit()            

values=[]
coords=[]
reader = csv.DictReader(open(inFile,'rb'),delimiter='\t')
for row in reader:
    if row.get(vCol)!='NA':
        values.append( float(row.get(vCol)) )
        coords.append((int(float(row.get(xCol))),int(float(row.get(yCol)))))

coords=n.asarray(coords,dtype=n.uint16)
values=n.asarray(values,dtype=n.float)
# Normalization to 1-65535, nonexisting will be alpha=0
if (imin==imax):
    values=values-values.min()
    values=(1+(65534*values/values.max()))
else:
    values=values-imin
    values=values/imax
    values[values>1]=1
    values[values<0]=0
    values=1+values*65534

# In case of empty CSV file, try to create a black image of given dimensions
if coords.shape[0] is not 0:
    if height==0:
        height=coords[:,1].max()+1
        inY=n.ones(coords.shape[0])
    else:
        inY=coords[:,1]<height
    if width==0:
        width=coords[:,0].max()+1
        inX=n.ones(coords.shape[0])
    else:
        inX=coords[:,0]<width

counter=n.zeros( (height,width) )
im=n.zeros( (height,width) )
alpha=n.zeros( (height,width) )

for c in range(0,coords.shape[0]):
    x=coords[c,0]
    y=coords[c,1]
    
    if (inX[c] and inY[c]):
        v=values[c]
        im[y,x]=im[y,x]+v
        alpha[y,x]=65535
        # change alpha to visible if point used.
        counter[y,x]=counter[y,x]+1

im[counter>0]=im[counter>0]/counter[counter>0]

ima=n.zeros( (height,width,2) )
ima[:,:,0]=im
ima[:,:,1]=alpha
ima=n.nan_to_num(ima).astype('uint16')

if color:
    imhsv=n.ones( (height,width,3)).astype('float')
    imhsv[:,:,0]=im.copy().astype('float')/65535
    # values not preset are colored black. (and transparent)
    imhsv[:,:,2]=n.where((alpha==65535),1,0)
    #alpha=im[:,:,1]
    ima=n.zeros((height,width,4)).astype('uint16')
    ima[:,:,3]=alpha
    ima[:,:,0:3]=(hsv2rgb(imhsv)*65535).astype('uint16')

imgtool.save_png(os.path.join(outDir,outFile+'.png'),
                ima,grayscale=(not color),bitdepth=16,alpha=True)

imgtool.save_png(freqDir+os.sep+outFile+'.png',
            counter.astype('uint8'),grayscale=True,bitdepth=8,alpha=False)




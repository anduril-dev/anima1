# Anima 

An add-on bundle for Anduril (http://www.anduril.org) project, that enables image analysis. Most of the low level tools require that you have Matlab installed! The bundle contains APIs for several image analysis/processing platforms, such as: Fiji, CellProfiler, Imagemagick

Further information: http://www.anduril.org/anima

This repository is compatible with Anduril 1.x series! See the other repository for Anduril 2.x compatibility https://bitbucket.org/anduril-dev/anima